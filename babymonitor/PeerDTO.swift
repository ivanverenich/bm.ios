//
//  PeerDTO.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class PeerDTO: NSObject {

    var displayName: String
    
    init(name: String) {
        displayName = name
        super.init()
    }
}
