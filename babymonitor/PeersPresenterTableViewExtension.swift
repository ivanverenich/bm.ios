//
//  PeersPresenterTableViewExtension.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

let noPeersCellIdentifier = "noPeers"
let peerCellIdentifier = "peerCell"

extension PeersPresenter {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (peers.count == 0) {
            return 1
        }
        
        return peers.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell
        if (peers.count == 0) {
            cell = tableView.dequeueReusableCell(withIdentifier: noPeersCellIdentifier, for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: peerCellIdentifier, for: indexPath)
            let label = cell.viewWithTag(1) as! UILabel
            label.text = peers[indexPath.row].displayName
        }
        
        return cell
    }
}
