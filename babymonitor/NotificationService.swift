//
//  NotificationService.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

protocol NotificationServiceProtocol {
    
    func notify(event: NotificationEvent)
    func notifyOnlyVibro()
}

enum NotificationEvent: Int {
    case childDeviceDisconnected
    case noise
    case childWhining
    case childCrying
    case childLaugh
    case silence
    case motion
}

class NotificationService: NSObject, NotificationServiceProtocol {

    var settings: SettingsManager?
    var soundNotifier: NotoficatorProtocol?
    var flashNotifier: NotoficatorProtocol?
    var vibroNotifier: NotoficatorProtocol?
    
    func notifyOnlyVibro() {
        
        if (vibroNotifier != nil) {
            vibroNotifier!.notify()
        }
    }
    
    func notify(event: NotificationEvent) {
        
        if (event == .childDeviceDisconnected) {
            vibroNotifier!.notify()
            flashNotifier!.notify()
            //soundNotifier!.notify()
        } else {
            
            var modeSettings : ModeSettingsDTO!
            
            switch settings!.currentMode {
            case .Sensitive:
                modeSettings = settings!.sensitiveModeSettings
            case .Medium:
                modeSettings = settings!.mediumModeSettings
            case .OnlyCrying:
                modeSettings = settings!.onlyCryingModeSettings
            default:
                fatalError("unknown settings mode")
            }
            
            if (modeSettings.enableFlash) {
                flashNotifier!.notify()
            }
            if (modeSettings.enableSound) {
                soundNotifier!.notify()
            }
            if (modeSettings.enableVibration) {
                vibroNotifier!.notify()
            }
        }
    }
}
