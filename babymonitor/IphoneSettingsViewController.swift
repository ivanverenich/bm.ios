//
//  IphoneSettingsViewController.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class IphoneSettingsViewController: UIViewController, SettingsViewProtocol {
    
    @IBOutlet weak var enableRemoteView: UIView!
    @IBOutlet weak var enableVideoView: UIView!
    @IBOutlet weak var enableRemoteHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var enableVideoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var purchaseView: UIView!
    @IBOutlet weak var purchaseViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    @IBOutlet weak var enableRemoteSwitch: UISwitch!
    @IBOutlet weak var enableVideoSwitch: UISwitch!
    @IBOutlet weak var enableMotionDetect: UISwitch!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    
    var presenter: SettingsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        backButton.image = UIImage(named: "ArrowBack")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        if (presenter!.deviceDestation == .Parent) {
            enableRemoteView.isHidden = true
            enableVideoView.isHidden = true
            enableRemoteHeightConstraint.constant = 0
            enableVideoHeightConstraint.constant = 0
            purchaseView.isHidden = true
            purchaseViewHeightConstraint.constant = 0
        } else {
            if !presenter!.isFree {
                
                purchaseView.isHidden = true
                purchaseViewHeightConstraint.constant = 0
            } else {
                presenter!.requestProducts()
            }
        }

        enableRemoteSwitch.isOn = presenter!.isRemoteEnabled
        enableVideoSwitch.isOn = presenter!.isVideoEnabled
        enableMotionDetect.isOn = presenter!.isMotionDetectEnabled
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updatePurchaseData(productName: String, price: String) {
        
        self.productName.text = productName
        self.productPrice.text = price
        
    }
    
    func updateOnPurchased() {
        
        purchaseView.isHidden = true
        purchaseViewHeightConstraint.constant = 0
    }
    
    func updatedOnPurchaseError() {
    
        let alert = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                      message: NSLocalizedString("inAppPurchaseErrorPurchasing", comment: ""),
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                      style: UIAlertActionStyle.cancel,
                                      handler: nil))
        
        
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func connectedDevicesPressed(_ sender: Any) {
        
        presenter!.showPeersDialog()
    }
    
    @IBAction func sensitiveModeButtonPressed(_ sender: Any) {
        
        presenter!.showNotificationModeDialog(mode: NotificationMode.Sensitive)
    }
    
    @IBAction func mediumModePressed(_ sender: Any) {
        
        presenter!.showNotificationModeDialog(mode: NotificationMode.Medium)
    }
    
    @IBAction func onlyCryingModePressed(_ sender: Any) {
        
        presenter!.showNotificationModeDialog(mode: NotificationMode.OnlyCrying)
    }
    
    @IBAction func enableVideoValueChanged(_ sender: Any) {
        
        presenter!.isVideoEnabled = enableVideoSwitch.isOn
    }
    
    @IBAction func enableRemoteValueChanged(_ sender: Any) {
        
        presenter!.isRemoteEnabled = enableRemoteSwitch.isOn
    }
    
    @IBAction func enableMotionDetectValueChanged(_ sender: Any) {
    
        presenter!.isMotionDetectEnabled = enableMotionDetect.isOn
    }
    
    @IBAction func closeDialogButtonPressed(_ sender: Any) {
        
        presenter!.closeDialog()
    }
    
    @IBAction func feedbackButtonPressed(_ sender: Any) {
    
        self.presenter!.initFeedbackEmail()
    }
    
    @IBAction func upgradeButtonPressed(_ sender: Any) {
        
        if (self.productPrice.text?.characters.count == 0) { // we dont have a price, need disable purchase button
            return
        }
        
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("inAppPurchaseUpgrade", comment: ""),
                                      style: UIAlertActionStyle.default,
                                      handler: {(alert: UIAlertAction!) in
                                        
                          self.presenter!.upgrade()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("inAppPurchaseRestore", comment: ""),
                                      style: UIAlertActionStyle.default,
                                      handler: {(alert: UIAlertAction!) in
                                        
                        self.presenter!.restore()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                      style: UIAlertActionStyle.cancel,
                                      handler: {(alert: UIAlertAction!) in
                                        
                                        
        }))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController?.sourceView = purchaseView
            alert.popoverPresentationController?.sourceRect = purchaseView.frame
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentViewController(vc: UIViewController) {
        
        self.present(vc, animated: true, completion: nil)
    }
}
