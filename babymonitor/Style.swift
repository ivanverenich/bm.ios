//
//  Style.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

let appMainColor = UIColor.init(red: 69.0 / 255, green: 134.0 / 255, blue: 248.0 / 255, alpha: 1.0)
let okColor = UIColor.green
let errorColor = UIColor.init(red: 255.0/255,
                              green: 90.0/255,
                              blue: 90.0/255,
                              alpha: 1.0)

let borderColor = UIColor.init(red: 128.0/255,
                               green: 222.0/255,
                               blue: 234.0/255,
                               alpha: 1.0)
