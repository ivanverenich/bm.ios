//
//  ChooseDeviceWireframeProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol ChooseDeviceWireframeProtocol {
    
    func presentChildDialog(settings: SettingsManager)
    func presentParentDialog(settings: SettingsManager)
}
