//
//  VibroNotifier.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import AudioToolbox

class VibroNotifier: NSObject, NotoficatorProtocol {

    private func singleVibration() {
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    func notify() {
        
        let standartVibrationLength = 0.4
        let waitInterval = 0.25
        
        DispatchQueue.global().async {
            
            for _ in 0 ... 1 {
                
                self.singleVibration()
                Thread.sleep(forTimeInterval: standartVibrationLength)
                Thread.sleep(forTimeInterval: waitInterval)
            }
        }
    }
}
