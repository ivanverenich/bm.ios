//
//  NotificationModes.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

enum NotificationMode: Int {
    case Sensitive = 1
    case Medium = 2
    case OnlyCrying = 3
}

enum DeviceDestination: Int {
    case Parent
    case Child
    case NotDefined
}
