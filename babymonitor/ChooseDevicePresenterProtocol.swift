//
//  ChooseDevicePresenterProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol ChooseDevicePresenterProtocol {
    
    func chooseDevice(deviceDestination: DeviceDestination)
}
