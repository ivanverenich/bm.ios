//
//  VoiceActivityDetection.h
//  BeWarned
//
//  Created by Admin on 26.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.

#import <Foundation/Foundation.h>

@protocol SoundActivityCallBackDetectionProtocol <NSObject>

@optional
-(void)didDetected:(float*) buffer ofLength:(int)length;
-(void)didNotDetected;

@end

@protocol SoundActivityDetectionProtocol <NSObject>

@required
    @property (nonatomic, weak) id <SoundActivityCallBackDetectionProtocol> delegate;
    // gets or sets sensetivity mode (0 - sensetive, 1 - medium, 2 - only crying)
    @property (nonatomic, assign) NSInteger sensetivityMode;
    @property (assign, nonatomic) float minimalEnegryCoefficient;
    -(void)detectBuffer:(float*)inBuffer ofLength:(int)length energyBuffer:(double*)inEnergyBuffer ofLength:(int)inEnergyBufferLength;
    -(void)clear;

@end

/**
 Represents sound activity deletection
 */
@interface SoundActivityDetection : NSObject <SoundActivityDetectionProtocol>

-(instancetype)initWithBufferTimeInterval:(NSTimeInterval)interval;

/**
 Checks for potentially dangerous sound

 @param inBuffer             input buffer
 @param length               length of input buffer
 @param inEnergyBuffer       input energy buffer
 @param inEnergyBufferLength input energy buffer length
 */
-(void)detectBuffer:(float*)inBuffer ofLength:(int)length energyBuffer:(double*)inEnergyBuffer ofLength:(int)inEnergyBufferLength;

/**
 Clears buffer
 */
-(void)clear;

/**
 Delegate
 */
@property (nonatomic, weak) id <SoundActivityCallBackDetectionProtocol> delegate;

/**
 Minimal energy coefficient
 */
@property (assign, nonatomic) float minimalEnegryCoefficient;

// gets or sets sensetivity mode (0 - sensetive, 1 - medium, 2 - only crying)
@property (nonatomic, assign) NSInteger sensetivityMode;

@end
