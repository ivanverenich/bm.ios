//
//  SoundTools.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/1/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

import UIKit

class SoundTools: NSObject {
    
    class func getAvarageEnergy(buffer: [Float]) -> Double {
        
        var energy = 0.0
        
        for elem in buffer {

            energy += Double(elem) * Double(elem)
        }
        
        return energy
    }
    
    class func convertUnsafePointerToArray<T>(pointer: UnsafeMutablePointer<T>, length: Int) -> [T] {
        return Array(UnsafeBufferPointer(start: pointer, count: length))
    }
    
    class func convertArrayToUnsafePointer<T>(array: [T]) -> UnsafeMutablePointer<T> {
        
        let pointer: UnsafeMutablePointer<T> = UnsafeMutablePointer(mutating: array)
        return pointer
    }
    
    class func createUnsafePointerFromArray<T>(array: [T]) -> UnsafeMutablePointer<T> {
        
        let pointer = UnsafeMutablePointer<T>.allocate(capacity: array.count)
        for i in 0 ..< array.count {
            pointer[i] = array[i]
        }
        return pointer
    }
    
    class func melToFrequency(value: Double) -> Double {
        
        return (700.0 * (pow(10.0, value / 2595.0) - 1.0))
    }
    
    class func frequencyToMel(value: Double) -> Double {
        
        return (2595.0 * log10(1.0 + value / 700.0))
    }
    
    class func copyUnsafePointerBuffer<T>(pointer: UnsafeMutablePointer<T>, length: Int) -> UnsafeMutablePointer<T> {
        
        let pointerCopy = UnsafeMutablePointer<T>.allocate(capacity: length)
        for i in 0 ..< length {
            pointerCopy[i] = pointer[i]
        }
        return pointerCopy
    }
}
