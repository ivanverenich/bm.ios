//
//  AudioProcessor2.h
//  BeWarned
//
//  Created by ivan verenich on 10/1/15.
//  Copyright © 2015 ivan verenich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioProcessorProtocol.h"

@interface AudioProcessor : NSObject <AudioProcessorProtocol>

@property (nonatomic, strong) id <AudioProcessorCallBackProtocol> delegate;

- (instancetype)init;
- (void) start;
- (void) stop;

@end
