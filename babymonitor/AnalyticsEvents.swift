//
//  AnalyticsEvents.swift
//  babymonitor
//
//  Created by ivan verenich on 12/13/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

enum AnalyticsEvents : String {
    
    case ShareApp                       = "Share"                               // +
    case ChooseChildDevice              = "Choose_child_device"                 // +
    case ChooseParentDevice             = "Choose_parent_device"                // +
    case OpenedSettings                 = "Opened_settings"
    case OpenedConnectedDevices         = "Opened_connected_devices"
    case OpenedSettingsMode             = "Opened_settings_mode"
    case DetectionStopedAuto            = "Detection_stoped_automatically"      // +
    case AttemptToUpgrade               = "Attempt_to_upgrade"
    case ChangedSensitivityMode         = "Changed_sensitivity_mode"            // +
    case ParentSessionGotNotification   = "Parent_session_got_notification"
}
