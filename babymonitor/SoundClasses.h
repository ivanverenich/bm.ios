//
//  SoundClasses.h
//  BeWarned
//
//  Created by Admin on 09.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DangerousEvents) {
    
    Scream = 0,
    Whimper = 1,
    Noise = 2,
    Unknown = 3
};

@interface SoundClasses : NSObject

+(DangerousEvents) getDangerousEventByIndex:(NSInteger) classIndex;
+(NSInteger) getDangerousEventsCount;

@end
