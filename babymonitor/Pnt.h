//
//  Pnt.h
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//
// describes point in multidimensional space

#import <Foundation/Foundation.h>

/**
 Represents Point in multidimensional space
 */
@interface Pnt : NSObject

/**
 vector of point coordinate
 */
@property (nonatomic, readonly) NSArray<NSNumber*>* vector;

/**
 length on vector
 */
@property (assign, nonatomic, readonly) NSInteger length;

/**
 Constructor of point

 @param vector array of NSNumber
 @return Point object
 */
- (instancetype) initWithVector:(NSArray<NSNumber*>*)vector;

/**
 Gets the distance between two points

 @param pnt Point object
 @return distance
 */
- (double) getDistance:(Pnt*)pnt;

@end
