//
//  ArtificialRecognizerManagerProtocol.h
//  SoundMonitor
//
//  Created by ivan verenich on 11/10/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

#ifndef ArtificialRecognizerManagerProtocol_h
#define ArtificialRecognizerManagerProtocol_h

@protocol ArtificialRecognizerManagerProtocol <NSObject>

@required
-(NSUInteger) recognize:(double**) image;

@end

#endif /* ArtificialRecognizerManagerProtocol_h */
