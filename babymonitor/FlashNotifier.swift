//
//  FlashNotifier.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import AVFoundation

class FlashNotifier: NSObject, NotoficatorProtocol {
    
    private let avDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
    
    private func isAvailable() -> Bool {
        if let device = avDevice {
            return device.hasTorch
        }
        return false
    }
    
    private func setModeOn(isOn: Bool) {
        
        let device = avDevice!
        
        do {
            _ = try device.lockForConfiguration()
        } catch {
            print("error locking flash for configuration")
            return
        }
        
        if (isOn) {
            device.torchMode = AVCaptureTorchMode.on
            device.flashMode = AVCaptureFlashMode.on
        } else {
            device.torchMode = AVCaptureTorchMode.off
            device.flashMode = AVCaptureFlashMode.off
        }
        
        // unlock your device
        device.unlockForConfiguration()
    }

    func notify() {
        
        if (!isAvailable()) { return }
        
        let standartFlashingLength = 0.25
        let waitInterval = 0.25
        
        DispatchQueue.global().async {
            
            for _ in 0 ... 1 {
                
                self.setModeOn(isOn: true)
                Thread.sleep(forTimeInterval: standartFlashingLength)
                self.setModeOn(isOn: false)
                Thread.sleep(forTimeInterval: waitInterval)
            }
        }
        
    }
}
