//
//  MainInteractorAppStoreManager.swift
//  babymonitor
//
//  Created by Ivan Verenich on 8/8/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import Foundation
import StoreKit

extension MainInteractor {
    
    // MARK: in app purchase
    
    func loadInAppPurchaseProductsIfNeed() {
        
        guard let sett = settings,
               sett.isFree else {
            return
        }
        
        Products.store.requestProducts { success, products in
            if success {
                self.products = products!
            }
        }
    }
    
    func purchase() {
        
        self.analytics!.sendEvent(event: AnalyticsEvents.AttemptToUpgrade)
        
        if (products == nil) { return }
        if (products!.count == 0) { return }
        
        Products.store.buyProduct(products![0])
    }
    
    func restore() {
        
        Products.store.restorePurchases()
    }
    
    func successPurchaseHandler() {
        
        self.settings!.setAppIsPaid()
        self.settings!.save()
        
        if (trialVersionTimer != nil) {
            trialVersionTimer!.invalidate()
            trialVersionTimer = nil
        }
    }
    
    func errorPurchaseHandler() {
    }
}
