//
//  PeersWireframe.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class PeersWireframe: BaseWireFrame, PeersWireframeProtocol {

    func closeDialog() {
        
        self.navigationController!.popViewController(animated: true)
    }
}
