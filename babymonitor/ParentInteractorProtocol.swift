//
//  ParentInteractorProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol ParentInteractorProtocol {
    
    var delegate: ParentInteractorDelegate? { get set }
    var settings: SettingsManager? {get set }
}
