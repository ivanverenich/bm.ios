//
//  ModelConstants.h
//  BeWarned
//
//  Created by Admin on 18.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

// в этом файле опмсаны константы, которые используются во многих модулях
// специфические для каждого модуля константы будут описаны непосредственно в самом модуле

#ifndef BeWarned_ModelConstants_h
#define BeWarned_ModelConstants_h

#define SampleRate              44100   // частота дискритизации
#define FrameLength             512     // количество отсчетов в одном фрейме для временного анализа
#define HandlingBufferLengthSec 1       // длина буфера, подоваемого на анализ, в секундах
#define RecognizeLengthSec      1.0     // длина буфера, который будет распознаваться, в секундах
#define FeatureCoefficientCount 13      // количество коэффициентов признаков
#define NetworkCount            1       // количество НС, применяемых для распознавания
#define MicrophoneBufSize       1024    // размер буфера микрофона в количестве сэмплов
#define FrameDuration           0.025   // size of frame in seconds
#define ShiftDuration           0.020   // size of frame shift in seconds
#define FrameCountInSound       49      // frames in sound of needin length
#define BufferLength            2.0     // length of buffer, seconds
#define EnegryUpdateTime        1.0     // time of updating energy value in buffer, seconds

#endif
