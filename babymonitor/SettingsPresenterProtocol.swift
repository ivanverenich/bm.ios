//
//  SettingsPresenterProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol SettingsPresenterProtocol {
    
    var isFree: Bool { get }
    var deviceDestation: DeviceDestination? { get set }
    var isRemoteEnabled: Bool { get set }
    var isVideoEnabled: Bool { get set }
    var isMotionDetectEnabled: Bool { get set }
    func showPeersDialog()
    func showNotificationModeDialog(mode: NotificationMode)
    func closeDialog()
    func upgrade()
    func restore()
    func requestProducts()
    func initFeedbackEmail()
}
