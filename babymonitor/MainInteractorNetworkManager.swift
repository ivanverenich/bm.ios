//
//  MainInteractorNetworkManager.swift
//  babymonitor
//
//  Created by Ivan Verenich on 8/8/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import Foundation
import MultipeerConnectivity

extension MainInteractor: MPCManagerDelegate {
    
    func foundPeer(peerID: MCPeerID) {
    }
    
    func lostPeer(peerID: MCPeerID) {
    }
    
    func invitationWasReceived(fromPeer: String) {
        
        //print("did receive invatation")
        self.appDelegate.mpcManager.invitationHandler(true, self.appDelegate.mpcManager.session)
    }
    
    func connectedWithPeer(peerID: MCPeerID) {
    }
    
    func didLostAllActiveConnections() {
        
        if (settings!.enableRemote) {
            didLostConnectionDuringStreaming = true
        }
        stopVIdeoIfPossible()
    }
    
    func establishedActiveConnection() {
        
        didLostConnectionDuringStreaming = false
        startVideoIfPossible()
    }
    
    func didReceivedData(data: Data) {
    }
}
