//
//  FlowControl.m
//  BeWarned
//
//  Created by Admin on 02.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "SoundMonitorFlowControl.h"
#import "Buffer.h"
#import <AVFoundation/AVFoundation.h>
#import "ClusterizationManager.h"
#import "HMMManager.h"
#import "ModelConstants.h"
#import "SoundTecnique.h"

@interface SoundMonitorFlowControl()
    
// captures sound from device microphone
@property (nonatomic, strong) id<AudioProcessorProtocol> audioProcessor;

// sound buffer
@property (nonatomic, strong) id<SoundBufferProtocol> soundBuffer;
    
// object for detection the beginning of sound wave
@property (nonatomic, strong) id<SoundActivityDetectionProtocol> soundActivityDetection;

// recognition manager
@property (nonatomic, strong) id<RecognitionManagerProtocol> recognitionManager;
    
// the time of last update
@property (nonatomic, strong) NSDate* lastTimeUpdated;
    
// the time of last dog recognition
@property (nonatomic, strong) NSDate* lastTimeDogRecognized;
    
// loud of sound in dB
@property (nonatomic, assign) CGFloat dB;
    
// true, if recognition process was interrupted (by call, for example)
@property (nonatomic, assign) BOOL didInterrupted;

@property (nonatomic, strong) NSString* audioProcessorClassName;

@end

@implementation SoundMonitorFlowControl


- (void) setIsBackgroundMode:(BOOL)isBackgroundMode {
    
    _isBackgroundMode = isBackgroundMode;
    if (_isBackgroundMode) {
        
        if (_delegate && [_delegate respondsToSelector:@selector(didEnteredBackgroundAndContinuedWork)]) {
            [_delegate didEnteredBackgroundAndContinuedWork];
        }
    
    } else {
        if (_delegate) {
            if (_isTurnOn && [_delegate respondsToSelector:@selector(didEnteredForegroundAndContinueWork)]) {
                [_delegate didEnteredForegroundAndContinueWork];
            } else if ([_delegate respondsToSelector:@selector(didEnteredForegroundAndStopedWork)]) {
                [_delegate didEnteredForegroundAndStopedWork];
            }
        }
    }
}
    
- (void) setIsTurnOn:(BOOL)isTurnOn {
    
    _isTurnOn = isTurnOn;
    
    if (_isTurnOn) {
        [self setupStartAudioProcessor];
        
        if (!_didInterrupted) {
            [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(handleInterruption:)
                                                     name: AVAudioSessionInterruptionNotification
                                                   object: [AVAudioSession sharedInstance]];
        }
    
    } else {
        
        if (!_didInterrupted) [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self stopAudioProcessor];
    }
}
    
-(void) setSensetivity:(float)sensetivity {
    
    if (_sensetivity == sensetivity) { return; }
    _sensetivity = sensetivity;
    _soundActivityDetection.minimalEnegryCoefficient = 100 - _sensetivity;
}

- (void) setSensetivityMode:(NSInteger)sensetivityMode {
    
    if (_sensetivityMode == sensetivityMode) { return; }
    _sensetivityMode = sensetivityMode;
    _soundActivityDetection.sensetivityMode = _sensetivityMode;
}

-(id)init {
    
    if (self = [super init]) {
        
        [self defaultInit];
    }
    
    return self;
}

- (instancetype)initWithAudioProcessor:(id<AudioProcessorProtocol>)audioProcessor
                    recognitionManager:(id<RecognitionManagerProtocol>)recognitionManager
         soundActivityDetectionManager:(id<SoundActivityDetectionProtocol>)soundActivityDetection
                           soundBuffer:(id<SoundBufferProtocol>)soundBuffer {
    
    if (self = [super init]) {
        
        self.audioProcessor = audioProcessor;
        self.audioProcessor.delegate = self;
        self.audioProcessorClassName = NSStringFromClass([audioProcessor class]);
        
        _soundActivityDetection = soundActivityDetection;
        _soundActivityDetection.delegate = self;
        
        self.recognitionManager = recognitionManager;
        
        _soundBuffer = soundBuffer;
        _soundBuffer.delegate = self;
        
        [self defaultInit];
    }
    return self;
}

- (void) defaultInit {
    
    _sensetivityMode            = 0;
    _isTurnOn                   = NO;
    _lastTimeDogRecognized      = [NSDate date];
    _didInterrupted             = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"finishLoading" object:nil];
        
    });
}

# pragma mark - audio processor

-(void)setupStartAudioProcessor {
    
    if (_audioProcessor == nil) {
        
        _audioProcessor = [[NSClassFromString(self.audioProcessorClassName) alloc] init];
        _audioProcessor.delegate = self;
    }
    
    [_audioProcessor start];
}

-(void)stopAudioProcessor {
    
    [_audioProcessor stop];
    _audioProcessor.delegate = nil;
    _audioProcessor = nil;
}

# pragma handling interraptions

-(void) handleInterruption: (NSNotification *) notification {
    
    NSDictionary *interuptionDict = notification.userInfo;
    NSInteger interuptionType = [[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey] intValue];
    
    if (interuptionType == AVAudioSessionInterruptionTypeBegan) {
        
        _didInterrupted = YES;
        _isTurnOn = NO;
        
        if (_delegate && [_delegate respondsToSelector:@selector(didIterrupted)]) {
            [_delegate didIterrupted];
        }
        
    } else if (interuptionType == AVAudioSessionInterruptionTypeEnded) {
      
        _isTurnOn = YES;
        _didInterrupted = NO;
        
        if (_delegate && [_delegate respondsToSelector:@selector(didRestoreAfterIterruption)]) {
            [_delegate didRestoreAfterIterruption];
        }
    }
}

#pragma mark microphone events

-(void)OnDataAvailable:(SInt16*)data withLength:(int)length {
    
    [_soundBuffer updateBuffer:data withLength:length];
}

#pragma mark buffer events

-(void)DidHandlingBufferFill:(float*) buffer
                    OfLength:(int)length
                EnergyBuffer:(double*)energyBuffer
                    OfLength:(int)energyBufferLength {
    
    [_soundActivityDetection detectBuffer:buffer ofLength:length energyBuffer:energyBuffer ofLength:energyBufferLength];
}

-(void)DidUpdateBuffer:(float*) buffer OfLength:(int)length {

    free(buffer);
}

- (void) DidGetEnergy:(double)energy atTime:(NSDate *)time ofLength:(NSInteger)length {

}

#pragma mark VAD events

-(void)didDetected:(float*) buffer ofLength:(int)length {
    
    if (_sensetivityMode == 0) { // sensitive
        
        free(buffer);
        
        [_soundBuffer clear];
        [_soundActivityDetection clear];
        
        // stop recording from microphone
        [_audioProcessor stop];
        
        //if (!self.isBackgroundMode) {
            _audioProcessor.delegate = nil;
            _audioProcessor = nil;
        //}
        
        if ([self.delegate respondsToSelector:@selector(recognitionOfDangerousSoundDidCompleteWithParams:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableDictionary* recognitionParams  = [NSMutableDictionary new];
                
                // just some noise
                [recognitionParams setObject:@(2) forKey:@"DangerousEvent"];
                
                [self.delegate recognitionOfDangerousSoundDidCompleteWithParams:recognitionParams];
            });
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            if (self.isTurnOn) {
                [self setupStartAudioProcessor];
            }
        });
        
        return;
    }
    
    // stop recording from microphone
    [_audioProcessor stop];
    
    //if (!self.isBackgroundMode) {
        _audioProcessor.delegate = nil;
        _audioProcessor = nil;
    //}
    
    [_soundBuffer clear];
    
    // start recognition
    [self recognizeBuffer:buffer ofLength:length];
}

-(void)didNotDetected {
    
}

-(void)recognizeBuffer:(float*)buffer ofLength:(int)length {

    // clean buffers
    [_soundBuffer clear];
    [_soundActivityDetection clear];
    
    int frameDuration = [SoundTecnique ConvertToSamplesCountTime:FrameDuration];
    int frameShift = [SoundTecnique ConvertToSamplesCountTime:ShiftDuration];
    
    FeatureCollection* fc = [[FeatureCollection alloc] initWithBuffer:buffer
                                                                 OfLength:length
                                                            frameDuration:frameDuration
                                                               frameShift:frameShift];
    
    DangerousEvents recognozedEvent = [_recognitionManager recognize:fc];

    
    
    NSMutableDictionary* recognitionParams  = [NSMutableDictionary new];
    
    [recognitionParams setObject:@(recognozedEvent) forKey:@"DangerousEvent"];
    
    NSLog(@"dangerous event = %ld", (long)recognozedEvent);
    
    if ([self.delegate respondsToSelector:@selector(recognitionOfDangerousSoundDidCompleteWithParams:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate recognitionOfDangerousSoundDidCompleteWithParams:recognitionParams];
        });
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    
        if (_isTurnOn) {
            [self setupStartAudioProcessor];
        }
    });
}

@end
