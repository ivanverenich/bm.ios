//
//  MPCManager.swift
//  MPCRevisited
//
//  Created by Gabriel Theodoropoulos on 11/1/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

import UIKit
import MultipeerConnectivity

let appNetworkIdentifier = "baby-monitor"

protocol MPCManagerDelegate {
    
    func foundPeer(peerID: MCPeerID)
    func lostPeer(peerID: MCPeerID)
    func invitationWasReceived(fromPeer: String)
    func connectedWithPeer(peerID: MCPeerID)
    func didLostAllActiveConnections()
    func establishedActiveConnection()
    func didReceivedData(data: Data)
}

class MPCManager: NSObject, MCSessionDelegate, MCNearbyServiceBrowserDelegate, MCNearbyServiceAdvertiserDelegate {

    private var delegates: [MPCManagerDelegate] = []
    
    var session: MCSession!
    var outputStream: OutputStream?
    var browser: MCNearbyServiceBrowser!
    var advertiser: MCNearbyServiceAdvertiser!
    
    var foundPeers = [MCPeerID]()
    
    var invitationHandler: ((Bool, MCSession?)->Void)!
    private var connected: Bool = false
    var isConnected: Bool {
        get { return connected }
    }
    
    private var currentActiveConnections = 0
    
    override init() {
        super.init()
        
        let peer = MCPeerID(displayName: UIDevice.current.name)
        
        // session without encryption
        session = MCSession(peer: peer, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.none)
        
        // session with encryption by default
        //session = MCSession(peer: peer)
        
        session.delegate = self
        
        browser = MCNearbyServiceBrowser(peer: peer, serviceType: appNetworkIdentifier)
        browser.delegate = self
        
        advertiser = MCNearbyServiceAdvertiser(peer: peer, discoveryInfo: nil, serviceType: appNetworkIdentifier)
        advertiser.delegate = self
    }
    
    func connect() {
        
        browser.startBrowsingForPeers()
        advertiser.startAdvertisingPeer()
        
        connected = true
        
        print("connected")
    }
    
    func disconnect() {
        
        self.session.disconnect()
        self.foundPeers.removeAll()
        connected = false
        
        print("disconnected")
    }
    
    func addDelegate(delegate: MPCManagerDelegate) {
        
        var contains = false
        for (_, adelegate) in delegates.enumerated() {
            
            let tempadelegate = adelegate as! NSObject
            let tempdelegate = delegate as! NSObject
            
            if tempadelegate === tempdelegate {
                contains = true
                break
            }
        }
        
        if (!contains) {
            delegates.append(delegate)
        }
    }
    
    func removeDelegate(delegate: MPCManagerDelegate) {
        
        for (index, adelegate) in delegates.enumerated() {
            
            let tempadelegate = adelegate as! NSObject
            let tempdelegate = delegate as! NSObject
            
            if tempadelegate === tempdelegate {
                delegates.remove(at: index)
                break
            }
        }
    }
    
    // MARK: MCNearbyServiceBrowserDelegate method implementation
    
    // Found a nearby advertising peer.
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        
        var isNewPeer = true
        for (_, aPeer) in foundPeers.enumerated() {
            
            if aPeer == peerID {
                isNewPeer = false
                break
            }
        }
        
        if (isNewPeer) {
            foundPeers.append(peerID)
            for delegate in delegates {
                delegate.foundPeer(peerID: peerID)
            }
        }
    }

    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
     
        for (index, aPeer) in foundPeers.enumerated() {
            
                if aPeer == peerID {
                foundPeers.remove(at: index)
                break
            }
        }
        
        for delegate in delegates {
            delegate.lostPeer(peerID: peerID)
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print(error.localizedDescription)
    }
    
    // MARK: MCNearbyServiceAdvertiserDelegate method implementation
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Swift.Void) {
        
        self.invitationHandler = invitationHandler
        
        for delegate in self.delegates {
            delegate.invitationWasReceived(fromPeer: peerID.displayName)
        }
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print(error.localizedDescription)
    }
    
    // MARK: MCSessionDelegate method implementation
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state{
        case MCSessionState.connected:
            print("Connected to session: \(session)")
            
            if (currentActiveConnections == 0) {
                for delegate in delegates {
                    delegate.establishedActiveConnection()
                }
            }
            
            currentActiveConnections = session.connectedPeers.count
            
            for delegate in delegates {
                delegate.connectedWithPeer(peerID: peerID)
            }
            
        case MCSessionState.connecting:
            print("Connecting to session: \(session)")
            
        default:
            currentActiveConnections = session.connectedPeers.count
            if (currentActiveConnections == 0) {
                for delegate in delegates {
                    delegate.didLostAllActiveConnections()
                }
            }
            print("Did not connect to session: \(session)")
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
    
        for delegate in delegates {
            delegate.didReceivedData(data: data)
        }
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }
    
    // MARK: Custom method implementation
    
    func sendData(data: Data, toPeer targetPeers: [MCPeerID]) -> Bool {
        
        do {
            try session.send(data, toPeers: session.connectedPeers, with: MCSessionSendDataMode.reliable)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
}
