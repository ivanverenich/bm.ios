//
//  ModeSettingsPresenterProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol ModeSettingsPresenterProtocol {
    
    var mode: ModeSettingsDTO { get set }
    var deviceDestination : DeviceDestination! { get set }
    func closeDialog()
}
