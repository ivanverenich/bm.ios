//
//  ProductDTO.swift
//  babymonitor
//
//  Created by ivan verenich on 12/7/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ProductDTO: NSObject {

    var localizedName: String = ""
    var price: Double = 0
    var localizedPrice: String = ""
}
