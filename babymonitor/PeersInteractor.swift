//
//  PeersInteractor.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class PeersInteractor: NSObject, PeersInteractorProtocol {
    
    func getPeers() -> [PeerDTO] {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let peersList = appDelegate.mpcManager.foundPeers
        let peersListDTO: [PeerDTO] = peersList.map { (peer: MCPeerID) -> PeerDTO in
            
            let peerDTO : PeerDTO = PeerDTO(name: peer.displayName)
            return peerDTO
        }
        
        return peersListDTO
    }

}
