//
//  Buffer.h
//  BeWarned
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SoundBufferCallBackProtocol <NSObject>

@optional
-(void)DidHandlingBufferFill:(float*) buffer
                    OfLength:(int)length
                EnergyBuffer:(double*)energyBuffer
                    OfLength:(int)energyBufferLength;

-(void)DidUpdateBuffer:(float*) buffer OfLength:(int)length;
-(void)DidGetEnergy:(double)energyValue atTime:(NSDate*)time ofLength:(NSInteger)length;

@end

@protocol SoundBufferProtocol <NSObject>

@required
    @property (nonatomic, weak) id <SoundBufferCallBackProtocol> delegate;
    @property (nonatomic, readonly) float* buffer;
    @property (nonatomic, readonly) int bufferLength;
    @property (nonatomic, readonly) double* energyBuffer;
    @property (nonatomic, readonly) int energyBufferLength;

    -(void)updateBuffer:(SInt16*)data withLength:(int)length;
    -(void)updateBufferWithFloatData:(float*)data withLength:(int)length;
    -(void)clear;

@end

@interface SoundBuffer : NSObject <SoundBufferProtocol>

/**
 converted sound buffer
 */
@property (nonatomic, readonly) float* buffer;

/**
 Buffer length in samples count
 */
@property (nonatomic, readonly) int bufferLength;

/**
 energy buffer
 */
@property (nonatomic, readonly) double* energyBuffer;

/**
 energy buffer length
 */
@property (nonatomic, readonly) int energyBufferLength;

- (instancetype)initWithBufferLength:(NSTimeInterval)bufferTimeLength
                        updateLength:(int)updateLength
                    energyUpdateTime:(NSTimeInterval)energyUpdateTime;

-(void)updateBuffer:(SInt16*)data withLength:(int)length;       // updating buffer data
-(void)updateBufferWithFloatData:(float*)data withLength:(int)length;
-(void)clear;                                                   // clearing buffer
-(void) dealloc;

@property (nonatomic, weak) id <SoundBufferCallBackProtocol> delegate;

@end
