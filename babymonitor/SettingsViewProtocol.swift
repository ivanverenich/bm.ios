//
//  SettingsViewProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsViewProtocol {
    
    func updatePurchaseData(productName: String, price: String)
    func updateOnPurchased()
    func updatedOnPurchaseError()
    func presentViewController(vc: UIViewController)
}
