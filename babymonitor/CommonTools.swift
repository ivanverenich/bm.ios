//
//  CommonTools.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/2/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

class CommonTools: NSObject {
    
    class func Min <T : Comparable> (a: T, _ b: T) -> T {
        if a > b {
            return b
        }
        return a
    }
    
    class func Max <T : Comparable> (a: T, _ b: T) -> T {
        if a < b {
            return b
        }
        return a
    }
    
    class func convertNSNumberArrayToDoubleArray(sourceArray: [NSNumber]!)-> [Double] {
        
        var resultArray: [Double] = []
        for numObj in sourceArray {
            resultArray.append(numObj.doubleValue)
        }
        return resultArray
    }
    
    class func convertDoubleArrayToNSNumberArray(sourceArray: [Double])-> [NSNumber]! {
        
        var resultArray: [NSNumber]! = []
        for item in sourceArray {
            resultArray.append(NSNumber.init(value: item))
        }
        return resultArray
    }
}
