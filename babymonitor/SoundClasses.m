//
//  SoundClasses.m
//  BeWarned
//
//  Created by Admin on 09.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "SoundClasses.h"

@implementation SoundClasses

// возвращает имя класса по индексу
+(DangerousEvents) getDangerousEventByIndex:(NSInteger) classIndex {
    
    switch (classIndex) {
        case 0:
            return Scream;
        case 1:
            return Whimper;
        case 2:
            return Noise;
    }
    
    return Unknown;
}

+(NSInteger) getDangerousEventsCount {
    return 4;
}

@end
