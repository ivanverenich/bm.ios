//
//  SettingsInteractor.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import StoreKit

typealias ProductsRequestUICompletionHandler = (_ success: Bool, _ products: [ProductDTO]?) -> ()

class SettingsInteractor: NSObject, SettingsInteractorProtocol {
    
    var analytics: AnalyticsProtocol?
    var settings: SettingsManager?
    private var products: [SKProduct]?
    
    func getProductsWithComplitionHandler(handler: @escaping ProductsRequestUICompletionHandler) {
        
        Products.store.requestProducts { success, products in
            if success {
                
                self.products = products!
                
                let productsDTO = self.products!.map({ (product: SKProduct) -> ProductDTO in
                    
                    let productDTO = ProductDTO()
                    productDTO.localizedName = product.localizedTitle
                    productDTO.price = Double(product.price)
                    productDTO.localizedPrice = self.localizedPrice(price: Double(product.price), locale: product.priceLocale)
                    return productDTO
                })
                
                handler(true, productsDTO)
                
            } else {
                
                handler(false, nil)
            }
            
        }
    }
    
    func purchase() {
        
        self.analytics!.sendEvent(event: AnalyticsEvents.AttemptToUpgrade)
        
        if (products == nil) { return }
        if (products!.count == 0) { return }
        
        Products.store.buyProduct(products![0])
    }
    
    func restore() {
        
        Products.store.restorePurchases()
    }
    
    func localizedPrice(price: Double, locale: Locale) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = locale
        return formatter.string(from: NSNumber.init(value: price))!
    }
    
    func getContactEmail() -> String {
        
        return "brilenkomeraya@gmail.com"
    }
    
    func getContactSubject() -> String {
    
        return "Baby monitor user feedback"
    }
}
