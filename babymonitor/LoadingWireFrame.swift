//
//  LoadingWireFrame.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/22/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

import UIKit

let mainControllerIdentifier = "main"
let chooseDeviceViewControllerIdentifier = "chooseDeviceVC"
let parentViewControllerIdentifier = "parentVC"

class LoadingWireFrame: BaseWireFrame {
    
    var isLoggedIn = false

    func installRootViewControllerIntoWindow(window: UIWindow) {
        
        NavigationControllerAppearance.setup()
        
        let settings = SettingsManager.loadSettings()
        switch settings.deviceDestination {
        case .Child:
            loadForChild(window: window, settings: settings)
        case .Parent:
            loadForParent(window: window, settings: settings)
        default:
            loadChoosing(window: window, settings: settings)
        }
    }
    
    func loadForChild(window: UIWindow, settings: SettingsManager) {
        
        let vc = mainControllerFromStoryboard()
        let navigationController = UINavigationController(rootViewController: vc)
        
        let mainWireFrame = MainWireframe()
        mainWireFrame.navigationController = navigationController
        
        let interactor = MainInteractor()
        if (!UIDevice.current.isSimulator) {
            //let videoOutput = CustomCaptureOutput()
            //interactor.videoCapture = VideoCapture(captureOutput: videoOutput)
            interactor.videoCapture = VideoCaptureWithMotionDetect()
        }
        let detectionService = DetectionService()
        interactor.detectionService = detectionService
        
        let notificationService = NotificationService()
        notificationService.flashNotifier = FlashNotifier()
        notificationService.vibroNotifier = VibroNotifier()
        notificationService.soundNotifier = SoundNotifier()
        notificationService.settings = settings
        interactor.notificationService = notificationService
        interactor.settings = settings
        
        // sound monitor
        let audioProcessor = AudioProcessor()
        
        let filePath = Bundle.main.path(forResource: "clusterizaztion", ofType: "xml")
        let clusterManager = ClusterizationManager.init(file: filePath)
        
        let hmmManager = HMMManager.init(clusterizationManager: clusterManager)
        hmmManager!.observationLength = UInt(FrameCountInSound)
        
        let recognitionManager = SoundRecognitionManager.init(recognazer: hmmManager)
        let soundActivityDetector = SoundActivityDetection.init(bufferTimeInterval: RecognizeLengthSec)
        
        let soundBuffer = SoundBuffer.init(bufferLength: BufferLength, updateLength: MicrophoneBufSize, energyUpdateTime: EnegryUpdateTime)
   
        let soundMonitor = SoundMonitorFlowControl(audioProcessor: audioProcessor,
                                                   recognitionManager: recognitionManager, soundActivityDetectionManager: soundActivityDetector, soundBuffer: soundBuffer)
        
        detectionService.soundMonitor = soundMonitor
        
        let mainPresenter = MainPresenter()
        
        interactor.delegate = mainPresenter
        interactor.analytics = Analytics()
        vc.presenter = mainPresenter
        mainPresenter.view = vc
        mainPresenter.wireframe = mainWireFrame
        mainPresenter.interactor = interactor
                
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func loadForParent(window: UIWindow, settings: SettingsManager) {
        
        let vc = parentViewControllerFromStoryboard()
        let navigationController = UINavigationController(rootViewController: vc)
        
        let wireFrame = ParentWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = ParentInteractor()
        interactor.settings = settings
        
        let notificationService = NotificationService()
        notificationService.flashNotifier = FlashNotifier()
        notificationService.vibroNotifier = VibroNotifier()
        notificationService.soundNotifier = SoundNotifier()
        notificationService.settings = settings
        interactor.notificationService = notificationService
        
        let presenter = ParentPresenter()
        
        vc.presenter = presenter
        
        interactor.delegate = presenter
        
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        presenter.view = vc
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func loadChoosing(window: UIWindow, settings: SettingsManager) {
        
        let vc = chooseDeviceControllerFromStoryboard()
        let navigationController = UINavigationController(rootViewController: vc)
        
        let wireFrame = ChooseDeviceWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = ChooseDeviceInteractor()
        interactor.settings = settings
        interactor.analytics = Analytics()
        let presenter = ChooseDevicePresenter()
        
        vc.presenter = presenter
        
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func chooseDeviceControllerFromStoryboard() -> ChooseDeviceViewController {
        
        let storyboard = LoadingWireFrame.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: chooseDeviceViewControllerIdentifier) as! ChooseDeviceViewController
        return vc
    }
    
    func mainControllerFromStoryboard() -> ViewController {
        
        let storyboard = LoadingWireFrame.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: mainControllerIdentifier) as! ViewController
        return vc
    }
    
    func parentViewControllerFromStoryboard() -> ParentMainViewController {
        
        let storyboard = LoadingWireFrame.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: parentViewControllerIdentifier) as! ParentMainViewController
        return vc
    }
}
