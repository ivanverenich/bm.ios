//
//  MainPresenterProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

protocol MainPresenterProtocol {
    
    var currentMode: NotificationMode { get set }
    func startStopDetection()
    func rotateCamera()
    func openSettings()
    func openSharing(view: UIView)
    func upgrade()
    func restore()
}
