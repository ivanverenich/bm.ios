//
//  VideoCaptureWithMotionDetect.swift
//  babymonitor
//
//  Created by ivan verenich on 1/13/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import UIKit
import GPUImage
import CoreMotion

protocol VideoCaptureCallBackProtocol: class {
    
    func didRenderImage(image: UIImage)
    func didDetectedMotion(rect: CGRect)
    func didNotDetectedMotion()
}

protocol VideoCaptureWithMotionDetectProtocol : BaseVideoCaptureProtocol {
    
    var delegate: VideoCaptureCallBackProtocol? { get set }
    var filterView: GPUImageView? { get }
    func rotateCamera()
}

class VideoCaptureWithMotionDetect: NSObject, VideoCaptureWithMotionDetectProtocol {

    // GPU image
    let motionDetector = GPUImageMotionDetector()
    let brightnessFilter = GPUImageBrightnessFilter()
    
    var videoCamera: GPUImageVideoCamera?
    var filterView: GPUImageView?
    
    // accelerometr
    var motionManager: CMMotionManager?

    // delegate
    weak var delegate: VideoCaptureCallBackProtocol?

    // accelerometr
    private var previousX : Double = 0
    private var previousY : Double = 0
    private var previousZ : Double = 0
    
    private var currentX : Double = 0
    private var currentY : Double = 0
    private var currentZ : Double = 0
    
    private var isDeviceMoving = true
    
    override init() {
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoCaptureWithMotionDetect.changeCameraOrientation), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        self.motionManager = CMMotionManager()
        self.setupFilter()
    }
    
    // MARK confirming to protocol

    func start() {
        
        self.startAccelerometer()
        self.videoCamera!.startCapture()
    }
    
    func stop() {
        
        self.videoCamera!.stopCapture()
        self.motionManager!.stopAccelerometerUpdates()
    }
    
    func rotateCamera() {
        
        if let camera = self.videoCamera {
            camera.rotateCamera()
        }
    }
    
    func setupFilter() {
        
        // WARNING: corrent ImageSize ONLY together with camera resolution
        let imageSize = CGSize.init(width: 640, height: 480)
        
        videoCamera = GPUImageVideoCamera.init(sessionPreset: AVCaptureSessionPreset640x480, cameraPosition: AVCaptureDevicePosition.front)
        videoCamera!.rotateCamera() // front or back
        self.changeCameraOrientation()
        
        videoCamera!.addTarget(motionDetector)
        videoCamera!.addTarget(brightnessFilter)
        let rect = CGRect.init(x: 0, y: 0, width: 100, height: 100)
        self.filterView = GPUImageView.init(frame: rect)
        videoCamera!.addTarget(filterView)
        
        videoCamera!.runBenchmark = true
        
        
        motionDetector.motionDetectionBlock = { (motionCentroid, motionIntensity, frameTime) in
            
            guard let delegate = self.delegate else {
                return
            }
            
            // rendering image
            if let image = self.brightnessFilter.imageFromCurrentFramebuffer(with: .up) {
                delegate.didRenderImage(image: image)
            }
            self.brightnessFilter.useNextFrameForImageCapture()
            
            if (motionIntensity > 0.01 && !self.isDeviceMoving)
            {
                let motionBoxWidth = 1500.0 * motionIntensity
                
                let x = round(imageSize.width * motionCentroid.x - motionBoxWidth / 2.0)
                let y = round(imageSize.height * motionCentroid.y - motionBoxWidth / 2.0)
                
                let motionRect = CGRect(x: x, y: y, width: motionBoxWidth, height: motionBoxWidth)
                delegate.didDetectedMotion(rect: motionRect)
                
            } else {
                
                delegate.didNotDetectedMotion()
            }
        }
    }
    
    func startAccelerometer() {
        
        guard self.motionManager!.isAccelerometerAvailable else {
            return
        }
        
        let queue = OperationQueue()

        self.motionManager!.accelerometerUpdateInterval = 0.1
        self.motionManager!.startAccelerometerUpdates(to: queue) {
            (data, error) in
            
            if (error == nil && data != nil) {
                
                self.currentX = data!.acceleration.x.roundTo(places: 2)
                self.currentY = data!.acceleration.y.roundTo(places: 2)
                self.currentZ = data!.acceleration.z.roundTo(places: 2)
                
                if (self.currentX != self.previousX || self.currentY != self.previousY || self.currentZ != self.previousZ) {
                    self.isDeviceMoving = true
                } else {
                    self.isDeviceMoving = false
                }
                
                self.previousX = self.currentX
                self.previousY = self.currentY
                self.previousZ = self.currentZ
            }
        }
    }
    
    func changeCameraOrientation() {
        
        switch UIDevice.current.orientation {
        case .landscapeLeft:
            videoCamera?.outputImageOrientation = .landscapeRight//.landscapeLeft
            brightnessFilter.setInputRotation(GPUImageRotationMode.rotate180, at: 0)
        case .landscapeRight:
            videoCamera?.outputImageOrientation = .landscapeLeft//.landscapeRight
        case .portrait:
            videoCamera?.outputImageOrientation = .portrait
        case .portraitUpsideDown:
            videoCamera?.outputImageOrientation = .portraitUpsideDown
        default:
            videoCamera?.outputImageOrientation = UIApplication.shared.statusBarOrientation
            
            //brightnessFilter.setInputRotation(GPUImageRotationMode., at: 0)
        }
    }
}
