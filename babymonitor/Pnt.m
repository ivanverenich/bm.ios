//
//  Pnt.m
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//


#import "Pnt.h"

@interface Pnt()

@property (nonatomic, strong) NSArray<NSNumber*>* vector;

@end

@implementation Pnt

- (instancetype) initWithVector:(NSArray<NSNumber*>*)vector {
    
    self = [super init];
    if (self) {
        
        _vector = vector;
        _length = vector.count;
    }
    
    return self;
}

- (double) getDistance:(Pnt*)pnt {
    
    double distance = 0;
    for (int i = 0; i < pnt.length; i++) {
        
        float diff = [self.vector[i] doubleValue] - [pnt.vector[i] doubleValue];
        distance += diff * diff;
    }
    
    return sqrtf(distance);
}

@end
