//
//  SoundRecognitionManager.h
//  BeWarned
//
//  Created by Admin on 28.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArtificialRecognizerManagerProtocol.h"
#import "FeatureCollection.h"
#import "SoundClasses.h"

@protocol RecognitionManagerProtocol <NSObject>

@optional

-(DangerousEvents)recognize:(id<FeatureCollectionProtocol>)featureCollection;

@end

@interface SoundRecognitionManager : NSObject <RecognitionManagerProtocol>

-(id)initWithRecognazer:(id<ArtificialRecognizerManagerProtocol>)recognizer;
-(DangerousEvents)recognize:(id<FeatureCollectionProtocol>)featureCollection;

@end
