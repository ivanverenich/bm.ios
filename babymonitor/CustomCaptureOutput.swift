//
//  CustomCaptureOutput.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

protocol CustomCaptureOutputCallBack {
    
    func onFrameReceived(frame: Data)
}

class CustomCaptureOutput: AVCaptureVideoDataOutput, AVCaptureVideoDataOutputSampleBufferDelegate {

    private var sampleQueue: DispatchQueue!
    var delegate: CustomCaptureOutputCallBack?
    var needToSendData: Bool = false
    let context = CIContext.init(options: nil)
    
    override init() {
        
        //sampleQueue = DispatchQueue(label: "VideoSampleQueue")
        sampleQueue = DispatchQueue.init(label: "VideoSampleQueue", qos: DispatchQoS.userInteractive, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit, target: nil)
        
        super.init()
        
        self.setSampleBufferDelegate(self, queue: sampleQueue)
        self.alwaysDiscardsLateVideoFrames = true
    }
    
    // MARK: confirming to AVCaptureVideoDataOutputSampleBufferDelegate
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
        let timestamp = CMTimeGetSeconds(CMSampleBufferGetPresentationTimeStamp(sampleBuffer))
        
        let cvImage = CMSampleBufferGetImageBuffer(sampleBuffer)
        
        let width = CVPixelBufferGetWidth(cvImage!)
        let height = CVPixelBufferGetHeight(cvImage!)
        let frame = CGRect.init(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height))
        let size = CGSize.init(width: 320, height: 320)
        
        let cropRect = AVMakeRect(aspectRatio: size, insideRect: frame)
        let ciImage = CIImage.init(cvPixelBuffer: cvImage!)
        let croppedImage = ciImage.cropping(to: cropRect)
        
        let scaleFilter = CIFilter.init(name: "CILanczosScaleTransform")
        scaleFilter!.setValue(croppedImage, forKey: "inputImage")
        scaleFilter!.setValue(NSNumber.init(value: 0.25), forKey: "inputScale")
        scaleFilter!.setValue(NSNumber.init(value: 1.0), forKey: "inputAspectRatio")
        let finalImage = scaleFilter!.value(forKey: "outputImage") as! CIImage
        let cgBackedImage = self.cgImageBackedImageWithCIImage(ciImage: finalImage)
        
        let imageData = UIImageJPEGRepresentation(cgBackedImage, 0.8)
        
        // maybe not always the correct input?  just using this to send current FPS...
        let inputPort = connection.inputPorts[0] as! AVCaptureInputPort
        let deviceInput = inputPort.input as! AVCaptureDeviceInput
        let frameDuration = deviceInput.device.activeVideoMaxFrameDuration
        
        var parametersDict: [String: AnyObject] = [:]
        
        parametersDict["image"] = imageData as AnyObject?
        parametersDict["timestamp"] = NSNumber.init(value: timestamp)
        parametersDict["framesPerSecond"] = NSNumber.init(value: frameDuration.timescale)
        
        let data = NSKeyedArchiver.archivedData(withRootObject: parametersDict)
        
        //let data = PropertyListSerialization.data(fromPropertyList: parametersDict, format: PropertyListSerialization.PropertyListFormat.binary, options: PropertyListSerialization.WriteOptions.)
        
        if (delegate != nil) {
            delegate!.onFrameReceived(frame: data)
        }
    }
    
    private func cgImageBackedImageWithCIImage(ciImage: CIImage) -> UIImage {
        
        let ref = context.createCGImage(ciImage, from: ciImage.extent)
        let image = UIImage.init(cgImage: ref!, scale: UIScreen.main.scale, orientation: UIImageOrientation.right)
    
        return image
    }
}
