//
//  AudioProcessor2.m
//  BeWarned
//
//  Created by ivan verenich on 10/1/15.
//  Copyright © 2015 ivan verenich. All rights reserved.
//

#import "AudioProcessor.h"
#import "EZMicrophone.h"
#import "ModelConstants.h"

@interface AudioProcessor() <EZMicrophoneDelegate>

@property (nonatomic, strong) EZMicrophone* microphone;

@end

@implementation AudioProcessor

- (id)init
{
    self = [super init];
    if (self) {
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *error;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        if (error)
        {
            NSLog(@"Error setting up audio session category: %@", error.localizedDescription);
        }
        [session setActive:YES error:&error];
        if (error)
        {
            NSLog(@"Error setting up audio session active: %@", error.localizedDescription);
        }
        
        AudioStreamBasicDescription audioFormat;
        audioFormat.mSampleRate			= SampleRate;
        audioFormat.mFormatID			= kAudioFormatLinearPCM;
        audioFormat.mFormatFlags		= kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
        audioFormat.mFramesPerPacket	= 1;
        audioFormat.mChannelsPerFrame	= 1;
        audioFormat.mBitsPerChannel		= 16;
        audioFormat.mBytesPerPacket		= 2;
        audioFormat.mBytesPerFrame		= 2;
        
        _microphone = [[EZMicrophone alloc] initWithMicrophoneDelegate:self withAudioStreamBasicDescription:audioFormat];
        
        NSArray *inputs = [EZAudioDevice inputDevices];
        
        // trying using back microphone
        for (EZAudioDevice* input in inputs) {
            
            if ([input.dataSource.orientation isEqual:AVAudioSessionOrientationBack]) {
                [self.microphone setDevice:input];
                break;
            }
            
            //NSLog(@"%@", input.description);
        }
    }
    return self;
}

- (void) start {
    
    [self.microphone startFetchingAudio];
}

- (void) stop {
    
    [self.microphone stopFetchingAudio];
}

# pragma mark EZMicrophone delegate

-(void)    microphone:(EZMicrophone *)microphone
        hasBufferList:(AudioBufferList *)bufferList
       withBufferSize:(UInt32)bufferSize
 withNumberOfChannels:(UInt32)numberOfChannels {
    
    [self processBuffer:bufferList];
       
    // clean up the buffer
    //free(bufferList->mBuffers[0].mData);
    
}

-(void)processBuffer: (AudioBufferList*) audioBufferList {
    
    SInt16 *editBuffer = audioBufferList->mBuffers[0].mData;
    int bufLength = audioBufferList->mBuffers[0].mDataByteSize / 2;
    SInt16* arr = (SInt16*)malloc(sizeof(SInt16) * bufLength);
    memcpy(arr, editBuffer, sizeof(SInt16) * bufLength);
    [self.delegate OnDataAvailable:arr withLength:bufLength];
}

- (void)microphone:(EZMicrophone *)microphone changedPlayingState:(BOOL)isPlaying {
    
    //NSLog(@"%hhd", isPlaying);
    
}

@end
