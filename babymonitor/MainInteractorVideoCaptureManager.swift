//
//  MainInteractorVideoCaptureManager.swift
//  babymonitor
//
//  Created by Ivan Verenich on 8/8/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import Foundation
import UIKit

extension MainInteractor: VideoCaptureCallBackProtocol {
    
    var motionDelayTimeInterval: TimeInterval { return 3.0 }
    
    func didRenderImage(image: UIImage) {
        
        if frameCounter == 0 {
            self.prepareAndSendImage(image)
            frameCounter += 1
        } else if (frameCounter > 5) {
            frameCounter = 0
        } else {
            frameCounter += 1
        }
    }
    
    func didDetectedMotion(rect: CGRect) {
        
        motionRect = rect
        motionDetectTime = Date()
        
        //print("Rect \(rect)")
    }
    
    func didNotDetectedMotion() {
        
        let now = Date()
        guard let motionDetectTime = self.motionDetectTime,
                now.secondsBetween(motionDetectTime) >= motionDelayTimeInterval else {
            return
        }
        
        self.motionRect = nil
        self.motionDetectTime = nil
    }
    
    // MARK: Private methods
    
    private func prepareAndSendImage(_ image: UIImage) {
        
        let networkData = createNetworkPackage(image)
        
        let peers = self.appDelegate.mpcManager.foundPeers
        if (peers.count == 0) { return }
        if self.appDelegate.mpcManager.sendData(data: networkData, toPeer: peers) {
            // do on sent
        } else {
            // do if not sent
        }
    }
    
    private func createNetworkPackage(_ image: UIImage) -> Data {
    
        let imageData = UIImageJPEGRepresentation(image, 0.8)
        var parametersDict: [String: AnyObject] = [:]
    
        parametersDict["image"] = imageData as AnyObject?
        parametersDict["framesPerSecond"] = NSNumber.init(value: 1.0)
        
        if let motionRect = self.motionRect,
            settings!.isMotionDetectOn {
            
            //print("Motion rect = \(motionRect)")
            parametersDict["motionRect"] = NSValue.init(cgRect: motionRect) 
        }
        
        let sensitivityModeCode = NSNumber.init(value: settings!.currentMode.rawValue)
        parametersDict["sensitivityModeCode_motion"] = sensitivityModeCode
        
        let data = NSKeyedArchiver.archivedData(withRootObject: parametersDict)
    
        return data
    }
}
