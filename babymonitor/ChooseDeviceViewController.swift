//
//  ChooseDeviceViewController.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ChooseDeviceViewController: UIViewController {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var childView: UIView!
    
    var presenter: ChooseDevicePresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.layoutIfNeeded()
        
        parentView.layer.cornerRadius = parentView.bounds.height / 2
        parentView.layer.masksToBounds = true
        parentView.layer.borderWidth = 6.0
        parentView.layer.borderColor = borderColor.cgColor
        
        childView.layer.cornerRadius = childView.bounds.height / 2
        childView.layer.masksToBounds = true
        childView.layer.borderWidth = 6.0
        childView.layer.borderColor = borderColor.cgColor
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context: UIViewControllerTransitionCoordinatorContext) in
            
            self.rotated()
            
        }) { (context: UIViewControllerTransitionCoordinatorContext) in
            self.rotated()
        }
    }
    
    func rotated() {
        
        parentView.layer.cornerRadius = parentView.bounds.height / 2
        childView.layer.cornerRadius = childView.bounds.height / 2
    }

    @IBAction func parentButtonPressed(_ sender: Any) {
        
        presenter!.chooseDevice(deviceDestination: DeviceDestination.Parent)
    }
    
    @IBAction func childButtonPressed(_ sender: Any) {
    
        presenter!.chooseDevice(deviceDestination: DeviceDestination.Child)
    }
}
