//
//  FloatExtension.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/3/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

extension Float {
    
    static var MIN = Float.leastNormalMagnitude
    static var MAX = Float.greatestFiniteMagnitude
    
}
