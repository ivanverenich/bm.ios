//
//  ParentViewProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

protocol ParentViewProtocol {
    
    func updateStatusConnectionLost()
    func updateStatusConnectionEstablished()
    func updateRemoteVideoImage(image: UIImage, motionRect: CGRect?)
    func stopedReceiveingRemoteImage()
    func didReceiveRemoteEvent(event: NotificationEvent)
    func didReceiveTrialCounter(timeLeft: Int)
}
