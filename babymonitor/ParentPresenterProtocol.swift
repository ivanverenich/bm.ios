//
//  ParentPresenterProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

protocol ParentPresenterProtocol {

    func showSettingsDialogs()
}
