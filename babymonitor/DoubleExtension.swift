//
//  DoubleExtension.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/1/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

extension Double {
    
    static var MIN = Double.leastNormalMagnitude
    static var MAX = Double.greatestFiniteMagnitude

    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
