//
//  ViewController.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import AVFoundation
import GPUImage

let selectedBgColor = UIColor.init(red: 178.0/255,
                                 green: 246.0/255,
                                 blue: 247.0/255,
                                 alpha: 1.0)
let selectedLabelColor = UIColor.init(red: 114.0/255,
                                      green: 178.0/255,
                                      blue: 186.0/255,
                                      alpha: 1.0)

class ViewController: UIViewController, MainViewProtocol {

    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
  
    @IBOutlet weak var sensitiveView: UIView!
    @IBOutlet weak var sensitiveLabel: UILabel!
    @IBOutlet weak var mediumView: UIView!
    @IBOutlet weak var mediumLabel: UILabel!
    @IBOutlet weak var onlyCryingView: UIView!
    @IBOutlet weak var onlyCryingLabel: UILabel!
    
    
    @IBOutlet weak var videoPreview: GPUImageView!
    
    // trial version dialog
    @IBOutlet weak var trialView: UIView!
    @IBOutlet weak var trialLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var upgradeButton: UIButton!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var closeTrialDialogButton: UIButton!
    @IBOutlet weak var trialLabelMainForm: UILabel!
    
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    var aminationTimer: Timer?
    var presenter: MainPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsButton.image = UIImage(named: "settings_icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        shareButton.image = UIImage(named: "share_icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        trialView.isHidden = true
        trialLabelMainForm.isHidden = true
        
        progressView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.layoutIfNeeded()
        
        startView.layer.cornerRadius = startView.bounds.height / 2
        startView.layer.masksToBounds = true
        startView.layer.borderWidth = 6.0
        startView.layer.borderColor = borderColor.cgColor
        
        updateSwitchPanel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        rotated()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context: UIViewControllerTransitionCoordinatorContext) in
            
            self.rotated()
            
        }) { (context: UIViewControllerTransitionCoordinatorContext) in
            self.rotated()
        }
    }
    
    func rotated() {
        
        startView.layer.cornerRadius = startView.bounds.height / 2
    }

    @IBAction func startButtonPressed(_ sender: Any) {
        
        presenter!.startStopDetection()
    }

    @IBAction func settingsButtonPressed(_ sender: Any) {
        
        self.presenter!.openSettings()
    }
    
    @IBAction func sensitiveButtonPressed(_ sender: Any) {
        
        presenter!.currentMode = .Sensitive
        updateSwitchPanel()
    }
    
    @IBAction func mediumButtonPressed(_ sender: Any) {
        
        presenter!.currentMode = .Medium
        updateSwitchPanel()
    }
    
    @IBAction func onlyCryingButtonPressed(_ sender: Any) {
        
        presenter!.currentMode = .OnlyCrying
        updateSwitchPanel()
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
    
        self.presenter!.openSharing(view: self.view)
    }
    
    @IBAction func upgradeButtonPressed(_ sender: Any) {
        
        trialView.isHidden = true
        self.presenter!.upgrade()
    }
    
    @IBAction func restoreButtonPressed(_ sender: Any) {
    
        trialView.isHidden = true
        self.presenter!.restore()
    }
    
    @IBAction func closeTrialButtonPressed(_ sender: Any) {
        
        trialView.isHidden = true
    }
    
    @IBAction func rotateCameraButtonPressed(_ sender: Any) {
        
        presenter?.rotateCamera()
    }
    
    
    func onTimerTick() {
        
        progressView.progress = progressView.progress + 0.05
        if progressView.progress >= 1.0 {
            progressView.progress = 0
        }
    }
    
    func updateSwitchPanel() {
        
        switch presenter!.currentMode {
        case .Sensitive:
            sensitiveView.backgroundColor = selectedBgColor
            sensitiveLabel.textColor = selectedLabelColor
            
            mediumView.backgroundColor = UIColor.clear
            mediumLabel.textColor = UIColor.white
            
            onlyCryingView.backgroundColor = UIColor.clear
            onlyCryingLabel.textColor = UIColor.white
            
        case .Medium:
            
            sensitiveView.backgroundColor = UIColor.clear
            sensitiveLabel.textColor = UIColor.white
            
            mediumView.backgroundColor = selectedBgColor
            mediumLabel.textColor = selectedLabelColor
            
            onlyCryingView.backgroundColor = UIColor.clear
            onlyCryingLabel.textColor = UIColor.white
            
        case .OnlyCrying:
            
            sensitiveView.backgroundColor = UIColor.clear
            sensitiveLabel.textColor = UIColor.white
            
            mediumView.backgroundColor = UIColor.clear
            mediumLabel.textColor = UIColor.white
            
            onlyCryingView.backgroundColor = selectedBgColor
            onlyCryingLabel.textColor = selectedLabelColor

        }
    }
    
    // MARK: - MainViewProtocol
    func updateInterface(isDetecting: Bool) {
        
        if (isDetecting) {
            progressView.isHidden = false
            startButton.setTitle(NSLocalizedString("stop", comment: ""), for: .normal)
            
            progressView.progress = 0
            aminationTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                  target: self,
                                                  selector: #selector(ViewController.onTimerTick),
                                                  userInfo: nil,
                                                  repeats: true)
        } else {
            
            aminationTimer!.invalidate()
            aminationTimer = nil
            
            startButton.setTitle(NSLocalizedString("start", comment: ""), for: .normal)
            progressView.isHidden = true
            
            trialView.isHidden = true
            trialLabelMainForm.isHidden = true
        }
    }
    
    func addVideoPreview(layer: AVCaptureVideoPreviewLayer) {
        
        DispatchQueue.main.async {
            self.videoPreview.layer.addSublayer(layer)
        }
    }
    
    func removeVideoPreview(layer: AVCaptureVideoPreviewLayer) {
        
        DispatchQueue.main.async {
            layer.removeFromSuperlayer()
        }
    }
    
    func addVideoPreview(view: UIView) {
        
        DispatchQueue.main.async {
            self.videoPreview.addSubview(view)
        }
    }
    
    func removeVideoPreview(view: UIView) {
        
        DispatchQueue.main.async {
            view.removeFromSuperview()
        }
    }
    
    func startedWithTrialInterval() {
        
        timeLabel.text = "30:00"
        trialView.isHidden = false
        trialLabelMainForm.isHidden = false
    }
    
    func updateTrialTimeLeft(seconds: Int) {
        
        let minutes = seconds / 60
        let seconds = seconds - minutes * 60
        
        let minutesString = String.init(format: "%02d", minutes)
        let secondsString = String.init(format: "%02d", seconds)
        
        timeLabel.text = "\(minutesString):\(secondsString)"
        trialLabelMainForm.text = timeLabel.text
    }
    
    func stopedWithTrialInterval() {
        
        self.updateInterface(isDetecting: false)
    }
    
    func updateCapturedImage(image: UIImage, motionRect: CGRect?) {

    }
}

