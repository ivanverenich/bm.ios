//
//  ParentInteractorNotificationsExtension.swift
//  babymonitor
//
//  Created by ivan verenich on 12/7/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

extension ParentInteractor {
    
    func appWillEnterBackgroundNotificationHandler() {

        self.appDelegate.mpcManager.disconnect()
    }
    
    func appWillEnterForegroundNotificationHandler() {
        
        if (!self.appDelegate.mpcManager.isConnected) {
            self.appDelegate.mpcManager.connect()
        }
    }
}
