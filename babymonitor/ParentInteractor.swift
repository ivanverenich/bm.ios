//
//  ParentInteractor.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import MultipeerConnectivity

protocol ParentInteractorDelegate: class {
    
    func didLostAllActiveConnections()
    func establishedActiveConnection()
    func didReceiveRemoteImage(image: UIImage, motionRect: CGRect?)
    func stopedReceiveingRemoteImage()
    func didReceiveRemoteEvent(event: NotificationEvent)
    func didReceiveTrialCounter(timeLeft: Int)
}

let timerTriggerTime = 30.0
let timerVideoTriggerTime = 3.0

class ParentInteractor: NSObject, ParentInteractorProtocol, MPCManagerDelegate {

    var notificationService: NotificationServiceProtocol?
    weak var delegate: ParentInteractorDelegate?
    var settings: SettingsManager?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // video timers
    var lastTimeReceiveVideo: Date?
    var videoTimer: Timer?
    
    // event timers
    var lastTimeReceiveEvent: Date?
    var eventsTimer: Timer?
    
    // motion detect
    var didDetectedMotion = false
    
    override init() {
        
        super.init()
        appDelegate.mpcManager.addDelegate(delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ParentInteractor.appWillEnterBackgroundNotificationHandler), name: NSNotification.Name(rawValue: appWillEnterBackgroundNotificationName), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ParentInteractor.appWillEnterForegroundNotificationHandler), name: NSNotification.Name(rawValue: appWillEnterForegroundNotificationName), object: nil)
    }
    
    // MARK:  MPCManagerDelegate
    
    func foundPeer(peerID: MCPeerID) {
        
        appDelegate.mpcManager.browser.invitePeer(peerID, to: appDelegate.mpcManager.session, withContext: nil, timeout: 60)
    }
    
    func lostPeer(peerID: MCPeerID) {
    }
    
    func invitationWasReceived(fromPeer: String) {
    }
    
    func connectedWithPeer(peerID: MCPeerID) {
    }
    
    func didLostAllActiveConnections() {
     
        destroyEventsTimer()
        destroyVideoTimer()
        notificationService!.notify(event: .childDeviceDisconnected)
        delegate?.didLostAllActiveConnections()
    }
    
    func establishedActiveConnection() {
        
        createTimer()
        createVideoTimer()

        delegate?.establishedActiveConnection()
    }
    
    func didReceivedData(data: Data) {
        
        let dict = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String: AnyObject]
        
        if let val = dict["notificationCode"] { // notification
            
            lastTimeReceiveEvent = Date()
            
            let eventCode = val as! NSNumber
            let event = NotificationEvent(rawValue: eventCode.intValue)
            
            let notificationModeCode = dict["sensitivityModeCode"] as! NSNumber
            let notificationMode = NotificationMode(rawValue: notificationModeCode.intValue)
            settings!.currentMode = notificationMode!
            if (delegate != nil) {
                
                notificationService!.notify(event: event!)
                delegate!.didReceiveRemoteEvent(event: event!)
            }
            
        } else if let val = dict["trialLeft"] { // trial counter
            
            let timeLeft = val as! NSNumber
            delegate?.didReceiveTrialCounter(timeLeft: timeLeft.intValue)
            
        } else if let val = dict["image"] { // video
        
            lastTimeReceiveVideo = Date()
            
            let image = UIImage.init(data: val as! Data, scale: 2.0)
            _ = dict["framesPerSecond"]
            
            var motionRect: CGRect?
            if let rectValue = dict["motionRect"] as? NSValue,
                settings?.isMotionDetectOn ?? false {
                motionRect = rectValue.cgRectValue
                
                if !didDetectedMotion {
                    didDetectedMotion = true
                    
                    notificationService!.notify(event: .motion)
                    delegate?.didReceiveRemoteEvent(event: .motion)
                    
                }
            } else {
                
                didDetectedMotion = false
            }
            
            if let sensitivityModeCode = dict["sensitivityModeCode_motion"] as? NSNumber {
                let notificationMode = NotificationMode(rawValue: sensitivityModeCode.intValue)
                settings!.currentMode = notificationMode!
            }
        
            delegate?.didReceiveRemoteImage(image: image!, motionRect: motionRect)
        }
    }
    
    // MARK: - events timer
    
    func createTimer() {
        
        DispatchQueue.main.sync {
            
            if (eventsTimer != nil) {
                eventsTimer!.invalidate()
                eventsTimer = nil
            }
            
            eventsTimer = Timer.scheduledTimer(timeInterval: timerTriggerTime,
                                               target: self,
                                               selector: #selector(ParentInteractor.onEventTimerTriggered),
                                               userInfo: nil,
                                               repeats: true)
        }
    }
    
    func destroyEventsTimer() {
        
        if (eventsTimer != nil) {
            eventsTimer!.invalidate()
            eventsTimer = nil
        }
    }
    
    func onEventTimerTriggered() {
     
        if (self.lastTimeReceiveEvent == nil) { return }
        let currentTime = Date()
        if (currentTime > self.lastTimeReceiveEvent!.addingTimeInterval(timerTriggerTime)) {
            delegate!.didReceiveRemoteEvent(event: NotificationEvent.silence)
        }
    }
    
    // MARK: - video timer
    
    func createVideoTimer() {
        
        DispatchQueue.main.sync {
            
            if (videoTimer != nil) {
                videoTimer!.invalidate()
                videoTimer = nil
            }
            
            videoTimer = Timer.scheduledTimer(timeInterval: timerVideoTriggerTime,
                                               target: self,
                                               selector: #selector(ParentInteractor.onViedoTimerTriggered),
                                               userInfo: nil,
                                               repeats: true)
        }
    }
    
    func destroyVideoTimer() {
        
        if (videoTimer != nil) {
            videoTimer!.invalidate()
            videoTimer = nil
        }
    }
    
    func onViedoTimerTriggered() {
        
        if (self.lastTimeReceiveVideo == nil) { return }
        let currentTime = Date()
        if (currentTime > self.lastTimeReceiveVideo!.addingTimeInterval(timerVideoTriggerTime)) {
            delegate!.stopedReceiveingRemoteImage()
        }
    }
}
