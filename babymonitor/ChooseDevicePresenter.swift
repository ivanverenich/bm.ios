//
//  ChooseDevicePresenter.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ChooseDevicePresenter: NSObject, ChooseDevicePresenterProtocol {

    var interactor: ChooseDeviceInteractorProtocol?
    var wireframe: ChooseDeviceWireframeProtocol?
    
    func chooseDevice(deviceDestination: DeviceDestination) {
        
        self.interactor!.setCurrentDeviceDestination(deviceDestation: deviceDestination)
        
        if (deviceDestination == .Child) {
            self.wireframe!.presentChildDialog(settings: interactor!.settings!)
        } else {
            self.wireframe!.presentParentDialog(settings: interactor!.settings!)
        }
    }
}
