//
//  ParentPresenter.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ParentPresenter: NSObject, ParentPresenterProtocol, ParentInteractorDelegate {
    
    var interactor: ParentInteractorProtocol?
    var wireframe: ParentWireframeProtocol?
    var view: ParentViewProtocol?
    
    // MARK: - ParentInteractorDelegate
    
    func didLostAllActiveConnections() {
        
        self.view!.updateStatusConnectionLost()
    }
    
    func establishedActiveConnection() {
        
        self.view!.updateStatusConnectionEstablished()
    }
    
    func didReceiveRemoteImage(image: UIImage, motionRect: CGRect?) {
        
        self.view!.updateRemoteVideoImage(image: image, motionRect: motionRect)
    }
    
    func stopedReceiveingRemoteImage() {
        self.view!.stopedReceiveingRemoteImage()
    }
    
    func didReceiveRemoteEvent(event: NotificationEvent) {
        self.view!.didReceiveRemoteEvent(event: event)
    }
    
    func didReceiveTrialCounter(timeLeft: Int) {
        
        self.view!.didReceiveTrialCounter(timeLeft: timeLeft)
    }
    
    // MARK: - Parent presenter protocol
    
    func showSettingsDialogs() {
        
        self.wireframe!.showSettingsDialogs(settings: interactor!.settings!)
    }
}
