//
//  DetectionService.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

protocol DetectionServiceCallBack {
    
    func didDetectedEvent(event: NotificationEvent)
}

protocol DetectionServiceProtocol {
    
    // sound monitor
    var soundMonitor: SoundMonitorProtocol? { get set }
    var notificationMode: NotificationMode { get set }
    var sensetivityLevel: Double { get set }
    var delegate: DetectionServiceCallBack? { get set }
    func start()
    func stop()
}

class DetectionService: NSObject, DetectionServiceProtocol, SoundMonitorCallbackProtocol {

    // sound monitor
    var soundMonitor: SoundMonitorProtocol? {
        didSet {
            soundMonitor!.delegate = self
        }
    }
    var notificationMode: NotificationMode = .Sensitive {
        didSet {
            if (notificationMode == .Sensitive) {
                self.soundMonitor!.sensetivityMode = 0
            } else if (notificationMode == .Medium) {
                self.soundMonitor!.sensetivityMode = 1
            } else {
                self.soundMonitor!.sensetivityMode = 2
            }
        }
    }
    var sensetivityLevel: Double = 100 {
        didSet {
            self.soundMonitor!.sensetivity = Float(sensetivityLevel)
        }
    }
    var delegate: DetectionServiceCallBack?
    private var timer: Timer?
    
    func start() {
     
        self.soundMonitor!.isTurnOn = true
        
//        timer = Timer.scheduledTimer(timeInterval: 10.0,
//                                     target: self,
//                                     selector: #selector(DetectionService.generateEvent),
//                                     userInfo: nil, repeats: true)
    }
    
    func stop() {
        
//        timer!.invalidate()
//        timer = nil
        
        self.soundMonitor!.isTurnOn = false
    }
    
    // MARK: - timer
    
    func generateEvent() {
        
        if (delegate != nil) {
            delegate!.didDetectedEvent(event: NotificationEvent.childCrying)
        }
    }
    
    // MARK: - SoundMonitorCallbackProtocol
    
    func recognitionOfDangerousSoundDidComplete(withParams params: NSMutableDictionary!) {
        
        let eventCodeObject = params["DangerousEvent"] as! NSNumber
        let eventCode = eventCodeObject.intValue
        
        if (eventCode == 2) { // just some noise
            
            if (delegate != nil) {
                delegate!.didDetectedEvent(event: NotificationEvent.noise)
            }
        } else if (eventCode == 0 || eventCode == 1) {
            if (delegate != nil) {
                delegate!.didDetectedEvent(event: NotificationEvent.childCrying)
            }
        }
    }
}
