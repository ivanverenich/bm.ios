//
//  BaseWireFrame.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/22/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

import UIKit

class BaseWireFrame: NSObject {
    
    var navigationController: UINavigationController?

    class func getMainStoryBoard() -> UIStoryboard {
        return getStoryboardByName(name: "Main")
    }
    
    class func getIpadStoryBoard() -> UIStoryboard {
        return getStoryboardByName(name: "Chat")
    }
    
    private class func getStoryboardByName(name: String) -> UIStoryboard {
        let storyboard = UIStoryboard(name: name, bundle: Bundle.main)
        return storyboard
    }
}
