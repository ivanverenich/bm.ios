//
//  SettingsPresenter.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import MessageUI

class SettingsPresenter: NSObject, SettingsPresenterProtocol {

    var deviceDestation: DeviceDestination?
    private var remoteEnabled: Bool = true
    private var videoEnabled: Bool = true
    private var motionDetectEnabled = true
    var isFree: Bool {
        get {
            return interactor!.settings!.isFree
        }
    }
    
    var wireframe: SettingsWireframeProtocol?
    var view: SettingsViewProtocol?
    var interactor: SettingsInteractorProtocol? {
        willSet(newValue) {
            remoteEnabled = newValue!.settings!.enableRemote
            videoEnabled = newValue!.settings!.enableVideo
            motionDetectEnabled = newValue!.settings?.isMotionDetectOn ?? false
        }
    }
    
    var isRemoteEnabled: Bool {
        get { return remoteEnabled }
        set(newValue) {
            remoteEnabled = newValue
            interactor!.settings!.enableRemote = remoteEnabled
            interactor!.settings!.save()
        }
    }
    
    var isVideoEnabled: Bool {
        get { return videoEnabled }
        set(newValue) {
            videoEnabled = newValue
            interactor!.settings!.enableVideo = videoEnabled
            interactor!.settings!.save()
        }
    }
    
    var isMotionDetectEnabled: Bool {
        get {
            return motionDetectEnabled
        }
        set(newValue) {
            motionDetectEnabled = newValue
            interactor!.settings!.isMotionDetectOn = newValue
            interactor!.settings!.save()
        }
    }
    
    override init() {
        
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsPresenter.successPurchaseHandler), name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsPresenter.errorPurchaseHandler), name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseErrorNotification), object: nil)
    }
    
    func successPurchaseHandler() {
        
        self.view!.updateOnPurchased()
    }
    
    func errorPurchaseHandler() {
        
        self.view!.updatedOnPurchaseError()
    }
    
    func showPeersDialog() {
        
        wireframe!.showPeersDialog()
    }
    
    func closeDialog() {
        
        NotificationCenter.default.post(name: NSNotification.Name(settingsDidChanged), object: interactor!.settings!)
        NotificationCenter.default.removeObserver(self)
        
        interactor!.settings!.save()
        self.wireframe!.closeDialog()
    }
    
    func showNotificationModeDialog(mode: NotificationMode) {
        
        let settings = interactor!.settings!
        
        switch mode {
        case .Sensitive:
            wireframe!.showModeSettingsDialog(mode: settings.sensitiveModeSettings!,
                                              deviceDestination: settings.deviceDestination)
        case .Medium:
            wireframe!.showModeSettingsDialog(mode: settings.mediumModeSettings!,
                                              deviceDestination: settings.deviceDestination)
        case .OnlyCrying:
            wireframe!.showModeSettingsDialog(mode: settings.onlyCryingModeSettings!,
                                              deviceDestination: settings.deviceDestination)
        default:
            fatalError("Unknown mode")
        }
    }
    
    func requestProducts() {
        
        self.interactor!.getProductsWithComplitionHandler { (success: Bool, products: [ProductDTO]?) in
            
            if (!success) {
                self.view!.updatePurchaseData(productName: NSLocalizedString("inAppPurchaseErrorRequestingProducts", comment: ""),
                                              price: "")
                return
            }
            
            if (products == nil) { return }
            
            let product = products![0]
            let productName = product.localizedName
            let productPrice = product.localizedPrice
            self.view!.updatePurchaseData(productName: productName, price: productPrice)
        }
    }
    
    func upgrade() {
        
        self.interactor!.purchase()
    }
    
    func restore() {
        
        self.interactor!.restore()
    }
}
