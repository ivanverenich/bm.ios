//
//  MainInteractorProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

protocol MainInteractorProtocol {
    
    var analytics: AnalyticsProtocol? { get set }
    var settings: SettingsManager? { get set }
    var notificationService: NotificationServiceProtocol? { get set }
    var isDetecting: Bool { get set }
    var videoCapture: VideoCaptureWithMotionDetectProtocol? { get set }
    
    
    func start()
    func stop()
    func rotateCamera()
    func getSharingViewController(view: UIView) -> UIActivityViewController?
    func updateNotificationMode(newMode: NotificationMode)
    func purchase()
    func restore()
}
