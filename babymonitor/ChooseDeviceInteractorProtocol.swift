//
//  ChooseDeviceInteractorProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol ChooseDeviceInteractorProtocol {
    
    var analytics: AnalyticsProtocol? { get set }
    var settings: SettingsManager? { get set }
    func setCurrentDeviceDestination(deviceDestation: DeviceDestination)
    
}
