//
//  ModeSettingsWireframe.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ModeSettingsWireframe: BaseWireFrame, ModeSettingsWireframeProtocol {

    func closeDialog() {
        self.navigationController!.popViewController(animated: true)
    }
}
