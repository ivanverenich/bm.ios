//
//  MainInteractorDetectionManager.swift
//  babymonitor
//
//  Created by Ivan Verenich on 8/8/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import Foundation

extension MainInteractor: DetectionServiceCallBack {
    
    func didDetectedEvent(event: NotificationEvent) {
        
        let peers = self.appDelegate.mpcManager.session.connectedPeers
        if (peers.count == 0 || !settings!.enableRemote) { // local notifications
            
            if (didLostConnectionDuringStreaming && settings!.enableRemote) {
                
                notificationService!.notifyOnlyVibro()
                
            } else {
                
                notificationService!.notify(event: event)
            }
            
        } else {
            
            let notificationCode = NSNumber.init(value: event.rawValue)
            let sensitivityModeCode = NSNumber.init(value: settings!.currentMode.rawValue)
            var parametersDict: [String: AnyObject] = [:]
            parametersDict["notificationCode"] = notificationCode
            parametersDict["sensitivityModeCode"] = sensitivityModeCode
            
            let data = NSKeyedArchiver.archivedData(withRootObject: parametersDict)
            
            if self.appDelegate.mpcManager.sendData(data: data, toPeer: peers) {
                // do on sent
            } else {
                // do if not sent
            }
        }
    }
}
