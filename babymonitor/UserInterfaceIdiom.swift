//
//  UserInterfaceIdiom.swift
//  babymonitor
//
//  Created by ivan verenich on 12/15/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

enum UIUserInterfaceIdiom : Int {
    
    case Unspecified
    case Phone // iPhone and iPod touch style UI
    case Pad // iPad style UI
}
