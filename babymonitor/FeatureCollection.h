//
//  FeatureCollection.h
//  BeWarned
//
//  Created by Admin on 01.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FeatureFrameProtocol <NSObject>

@required
    -(NSArray<NSNumber*>*)getFeatureCoefficietns;

@end

@protocol FeatureCollectionProtocol <NSObject>

@required
    @property (nonatomic, readonly) BOOL isNoisy;
    @property (nonatomic, readonly) NSMutableArray<id<FeatureFrameProtocol>>* featureFrames;
    -(double**) getFeatureImage;

@end

/**
 Represents feature collection
 */
@interface FeatureCollection : NSObject <FeatureCollectionProtocol>


/**
 Gets in collection noisy or not
 */
@property (nonatomic, readonly) BOOL isNoisy;


/**
 Gets feature frames
 */
@property (nonatomic, readonly) NSMutableArray<id<FeatureFrameProtocol>>* featureFrames;

/**
 Creates instance from buffer

 @param buffer sound buffer
 @param length length of sound buffer
 @param frameDuration frame duration
 @param frameShift frame shift

 @return self
 */
-(id)initWithBuffer:(float*) buffer OfLength:(int)length frameDuration:(int)frameDuration frameShift:(int)frameShift;

/**
 Gets feature image as matrix

 @return feature image
 */
-(double**) getFeatureImage;


@end
