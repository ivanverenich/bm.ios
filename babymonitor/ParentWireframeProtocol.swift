//
//  ParentWireframeProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol ParentWireframeProtocol {
    
    func showSettingsDialogs(settings: SettingsManager)
}
