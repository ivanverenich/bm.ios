//
//  SettingsWireframe.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

let peersViewControllerIdentifier = "peersVC"
let modeSettingsViewControllerIdentifier = "modeSettingsVC"

class SettingsWireframe: BaseWireFrame, SettingsWireframeProtocol {

    func showPeersDialog() {
        
        let peersVC = iphonePeersControllerFromStoryboard()
        
        let wireFrame = PeersWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = PeersInteractor()
        
        let presenter = PeersPresenter()
        peersVC.presenter = presenter
        presenter.view = peersVC
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        
        self.navigationController?.pushViewController(peersVC, animated: true)
    }
    
    func showModeSettingsDialog(mode: ModeSettingsDTO, deviceDestination: DeviceDestination) {
        
        let modeSettingsVC = modeSettingsControllerFromStoryboard()
        
        let wireFrame = ModeSettingsWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = ModeSettingsInteractor()
        
        let presenter = ModeSettingsPresenter.init(notificationMode: mode)
        modeSettingsVC.presenter = presenter
        presenter.view = modeSettingsVC
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        presenter.deviceDestination = deviceDestination
        
        self.navigationController?.pushViewController(modeSettingsVC, animated: true)
    }
    
    func closeDialog() {
        self.navigationController!.popViewController(animated: true)
    }
    
    func iphonePeersControllerFromStoryboard() -> PeersViewController {
        
        let storyboard = MainWireframe.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: peersViewControllerIdentifier) as! PeersViewController
        
        return vc
    }
    
    func modeSettingsControllerFromStoryboard() -> ModeSettingsViewController {
        
        let storyboard = MainWireframe.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: modeSettingsViewControllerIdentifier) as! ModeSettingsViewController
        
        return vc
    }
}
