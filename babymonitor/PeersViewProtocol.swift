//
//  PeersViewProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol PeersViewProtocol {
 
    func updatePeers()
}
