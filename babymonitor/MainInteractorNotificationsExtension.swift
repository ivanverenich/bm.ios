//
//  MainInteractorNotificationsExtension.swift
//  babymonitor
//
//  Created by ivan verenich on 12/7/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

extension MainInteractor {
    
    // MARK: - observing settings changed
    
    func settingsRemoteChanged(notification: Notification) {
        
        let object = notification.object as! NSNumber
        let remoteEnabled = object.boolValue
        
        if (settings!.enableVideo && remoteEnabled) {
            startVideoIfPossible()
        } else {
            stopVIdeoIfPossible()
        }
    }
    
    func settingsVideoChanged(notification: Notification) {
        
        let object = notification.object as! NSNumber
        let videoEnabled = object.boolValue
        if (videoEnabled && settings!.enableRemote) {
            startVideoIfPossible()
        } else {
            stopVIdeoIfPossible()
        }
    }
    
    func appWillEnterBackgroundNotificationHandler() {
        
        if !self.isDetecting {
            self.appDelegate.mpcManager.disconnect()
            self.detectionService!.soundMonitor!.isBackgroundMode = true
        }
    }
    
    func appWillEnterForegroundNotificationHandler() {
        
        if (!self.appDelegate.mpcManager.isConnected) {
            self.appDelegate.mpcManager.connect()
            self.detectionService!.soundMonitor!.isBackgroundMode = false
        }
    }
    
    func settingsDidChangedNotificationHandler(notification: Notification) {
     
        let settings = notification.object as? SettingsManager
        self.detectionService!.sensetivityLevel = Double(settings!.sensitiveModeSettings!.sensitivityValue!)
    }
}
