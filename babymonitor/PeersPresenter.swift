//
//  PeersPresenter.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class PeersPresenter: NSObject, PeersPresenterProtocol, MPCManagerDelegate {

    var interactor: PeersInteractorProtocol?
    var view: PeersViewProtocol?
    var wireframe: PeersWireframeProtocol?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var peers: [PeerDTO] = []

    override init() {
        
        super.init()
        appDelegate.mpcManager.addDelegate(delegate: self)
    }
    
    func getPeers() {
        peers = self.interactor!.getPeers()
    }
    
    func onClosing() {
        
        appDelegate.mpcManager.removeDelegate(delegate: self)
    }
    
    func closeDialog() {
        
        self.wireframe!.closeDialog()
    }
    
    // MARK: - MPCManagerDelegate
    
    func foundPeer(peerID: MCPeerID) {
        
        getPeers()
        self.view!.updatePeers()
    }
    
    func lostPeer(peerID: MCPeerID) {
        
        getPeers()
        self.view!.updatePeers()
    }
    
    func invitationWasReceived(fromPeer: String) {
        
    }
    
    func connectedWithPeer(peerID: MCPeerID) {
        
    }
    
    func didLostAllActiveConnections() {
        
    }
    
    func establishedActiveConnection() {
        
    }
    
    func didReceivedData(data: Data) {
        
    }
}
