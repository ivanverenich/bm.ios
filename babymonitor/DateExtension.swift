//
//  DateExtension.swift
//  babymonitor
//
//  Created by Ivan Verenich on 8/8/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import Foundation

extension Date {
    
    func secondsBetween(_ date: Date) -> TimeInterval {
        
        return self.timeIntervalSince(date)
    }
}
