//
//  NavigationControllerAppearance.swift
//  babymonitor
//
//  Created by ivan verenich on 12/12/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class NavigationControllerAppearance: NSObject {

    class func setup() {
        
//        let fontFamilies = UIFont.familyNames
//        
//        for i in 0 ..< fontFamilies.count {
//            
//            let fontFamily = fontFamilies[i]
//            let fontNames = UIFont.fontNames(forFamilyName: fontFamily)
//            print(fontFamily, fontNames)
//        }
        
        let navigationBarAppearace = UINavigationBar.appearance()
        let bgColor = UIColor.init(red: 178.0 / 255,
                                   green: 246.0 / 255,
                                   blue: 247.0/255,
                                   alpha: 1.0)
        
        navigationBarAppearace.tintColor = bgColor
        navigationBarAppearace.barTintColor = bgColor
        
        let titleTextColor = UIColor.init(red: 114.0 / 255,
                                          green: 178.0 / 255,
                                          blue: 186.0/255,
                                          alpha: 1.0)
        
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName: titleTextColor,
                                                      NSFontAttributeName: UIFont(name: "OpenSans-Bold", size: 24)!,
                                                      NSStrokeColorAttributeName : UIColor.white,
                                                      NSStrokeWidthAttributeName : NSNumber.init(value: -2.0)]
        
        
    }
}
