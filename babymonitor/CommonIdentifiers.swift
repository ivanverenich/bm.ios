//
//  CommonIdentifiers.swift
//  babymonitor
//
//  Created by ivan verenich on 12/7/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

let appWillEnterBackgroundNotificationName = "appWillEnterBackground"
let appWillEnterForegroundNotificationName = "appWillEnterForeground"
let settingsDidChanged = "settingsDidChanged"
