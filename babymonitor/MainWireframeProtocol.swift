//
//  MainWireframeProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

protocol MainWireframeProtocol {
    
    func openSettingsDialog(settings: SettingsManager)
    func presentSharingDialog(presenterVC: UIViewController, sharingVC: UIActivityViewController?)
}
