//
//  ChooseDeviceInteractor.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ChooseDeviceInteractor: NSObject, ChooseDeviceInteractorProtocol {

    var analytics: AnalyticsProtocol?
    var settings: SettingsManager?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func setCurrentDeviceDestination(deviceDestation: DeviceDestination) {
        
        if (deviceDestation == .Child) {
            self.analytics!.sendEvent(event: AnalyticsEvents.ChooseChildDevice)
        } else {
            self.analytics!.sendEvent(event: AnalyticsEvents.ChooseParentDevice)
        }
        
        settings?.deviceDestination = deviceDestation
        settings?.save()
        
        self.appDelegate.mpcManager.connect()
    }
}
