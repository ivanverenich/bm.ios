//
//  FlowControl.h
//  BeWarned
//
//  Created by Admin on 02.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioProcessor.h"
#import "AudioProcessorProtocol.h"
#import "SoundClasses.h"
#import "SoundRecognitionManager.h"
#import "AudioProcessorProtocol.h"
#import "Buffer.h"
#import "SoundActivityDetection.h"

// protocol allows to interact with UI
@protocol SoundMonitorCallbackProtocol <NSObject>

@optional

// notifies when recognition of dangerous sound is complete
-(void)recognitionOfDangerousSoundDidCompleteWithParams:(NSMutableDictionary*)params;

// notifies when system enters background and continues it work
-(void)didEnteredBackgroundAndContinuedWork;

// notifies when system enters foreground and continues it work
-(void)didEnteredForegroundAndContinueWork;

// notifies when system enters foreground and stops it work
-(void)didEnteredForegroundAndStopedWork;

// notifies when system iterruptes by external event (call, for example)
-(void)didIterrupted;

//  notifies when system restores after interruption
-(void)didRestoreAfterIterruption;

@end

@protocol SoundMonitorProtocol <NSObject>

// gets or sets is now background mode or not
@property (nonatomic, assign) BOOL isBackgroundMode;

// starts or stops system
@property (nonatomic, assign) BOOL isTurnOn;

// gets or sets sensitivity of system to load sounds
@property (nonatomic, assign) float sensetivity;

// gets or sets sensetivity mode (0 - sensetive, 1 - medium, 2 - only crying)
@property (nonatomic, assign) NSInteger sensetivityMode;

// delegate
@property (nonatomic, weak) id <SoundMonitorCallbackProtocol> delegate;

@end

@interface SoundMonitorFlowControl : NSObject <SoundMonitorProtocol, AudioProcessorCallBackProtocol, SoundBufferCallBackProtocol, SoundActivityCallBackDetectionProtocol>

// delegate
@property (nonatomic, weak) id <SoundMonitorCallbackProtocol> delegate;
    
// gets or sets is now background mode or not
@property (nonatomic, assign) BOOL isBackgroundMode;
    
// starts or stops system
@property (nonatomic, assign) BOOL isTurnOn;
    
// gets or sets sensitivity of system to load sounds
@property (nonatomic, assign) float sensetivity;

// gets or sets sensetivity mode (0 - sensetive, 1 - medium, 2 - only crying)
@property (nonatomic, assign) NSInteger sensetivityMode;

- (instancetype)initWithAudioProcessor:(id<AudioProcessorProtocol>)audioProcessor
                    recognitionManager:(id<RecognitionManagerProtocol>)recognitionManager
         soundActivityDetectionManager:(id<SoundActivityDetectionProtocol>)soundActivityDetection
                           soundBuffer:(id<SoundBufferProtocol>)soundBuffer;

@end
