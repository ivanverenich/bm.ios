//
//  DeviceExtension.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

extension UIDevice {
    var isSimulator: Bool {
//        #if IOS_SIMULATOR
//            return true
//        #else
//            return false
//        #endif
        
        return TARGET_OS_SIMULATOR != 0
    }
}
