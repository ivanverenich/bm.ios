//
//  Buffer.m
//  BeWarned
//
//  Created by Admin on 23.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Buffer.h"
#import "SoundTecnique.h"
#import "ModelConstants.h"

@interface SoundBuffer()
{
    
    int bufferCurrentPosition;          // current position in buffer
    int currentPositionEB;              // current position in energy buffer
    
    double* tempEnergyBuffer;            // buffer for temporal storage incoming data
    int tempEnergyBufferLength;         // length of temporal buffer
    
    NSDate *lastTimeEnergyValueUpdate;  // time of last update of energy
    int updateBufferLength;             // length of incoming buffer
}

@property (nonatomic, assign) NSTimeInterval energyUpdateTime;

/**
 converted sound buffer
 */
@property (nonatomic, assign) float* buffer;

/**
 Buffer length in samples count
 */
@property (nonatomic, assign) int bufferLength;

/**
 energy buffer
 */
@property (nonatomic, assign) double* energyBuffer;

/**
 energy buffer length
 */
@property (nonatomic, assign) int energyBufferLength;

@end

@implementation SoundBuffer

- (instancetype)initWithBufferLength:(NSTimeInterval)bufferTimeLength
                        updateLength:(int)updateLength
                    energyUpdateTime:(NSTimeInterval)energyUpdateTime {
    self = [super init];
    if (self) {
        
        updateBufferLength = updateLength;
        _energyUpdateTime = energyUpdateTime;
        
        // buffer setup
        _bufferLength = [SoundTecnique ConvertToSamplesCountTime:bufferTimeLength];
        _bufferLength = (_bufferLength / updateBufferLength + 1) * updateBufferLength;
        _buffer = (float *)malloc(sizeof(float) * _bufferLength);
        memset(_buffer, 0, sizeof(float) * _bufferLength);
        bufferCurrentPosition = 0;
        
        // energy buffer setup
        _energyBufferLength = _bufferLength / FrameLength;
        _energyBuffer = (double*)malloc(sizeof(double)*_energyBufferLength);
        memset(_energyBuffer, 0, sizeof(double) * _energyBufferLength);
        currentPositionEB = 0;
        
        // last update time
        lastTimeEnergyValueUpdate = [NSDate date];
        
        // temp buffers for handling incoming data
        tempEnergyBufferLength = MicrophoneBufSize / FrameLength;
        tempEnergyBuffer = (double*) malloc(sizeof(double)* tempEnergyBufferLength);
        memset(tempEnergyBuffer, 0, sizeof(double) * tempEnergyBufferLength);
        
    }
    return self;
}

-(void)clear {
    
    memset(_buffer, 0, sizeof(float) * _bufferLength);
    bufferCurrentPosition = 0;
    //
    memset(_energyBuffer, 0, sizeof(double) * _energyBufferLength);
    currentPositionEB = 0;
}

- (void) addEnergy:(double)energy {
    
    if (currentPositionEB > _energyBufferLength-1) {
        memmove(_energyBuffer, &_energyBuffer[1], sizeof(double) * (_energyBufferLength - 1) );
        currentPositionEB--;
    }
    
    _energyBuffer[currentPositionEB++] = energy;
}

- (void) addDataToBuffer:(float*)data ofLength:(int)length {
    
    if (bufferCurrentPosition >= _bufferLength - 1) {
    
        // allocating memory for new buffer
        float* bufferForHandling = (float*)malloc(sizeof(float) * _bufferLength);
        
        // copy data from working buffer to new buffer
        memcpy(bufferForHandling, _buffer, sizeof(float) * _bufferLength);
        
        // allocating memory for new energy buffer
        double* energyBufferForHandling = (double*)malloc(sizeof(double) * _energyBufferLength);
    
        // copy data from working energy buffer to new buffer
        memcpy(energyBufferForHandling, _energyBuffer, sizeof(double) * _energyBufferLength);
    
        memmove(_buffer, &_buffer[length], sizeof(float) * (_bufferLength - length));
        bufferCurrentPosition -= length;
    
        // delegating next handling
        if (_delegate && [_delegate respondsToSelector:@selector(DidHandlingBufferFill:OfLength:EnergyBuffer:OfLength:)]) {
            [self.delegate DidHandlingBufferFill:bufferForHandling
                                        OfLength:_bufferLength
                                    EnergyBuffer:energyBufferForHandling
                                        OfLength:_energyBufferLength];
        }
    }

    memcpy(&_buffer[bufferCurrentPosition], data, sizeof(float)*length);
    bufferCurrentPosition += length;
}

-(void)updateBuffer:(SInt16*)data withLength:(int)length {
   
    float* convertedBuffer = [self convertBufferToDouble:data withLength: length];
    [self updateBuffer:convertedBuffer length:length];
}

-(void)updateBufferWithFloatData:(float*)data withLength:(int)length {
    
    [self computeBufferEnegry:data length:length];
    [self updateBuffer:data length:length];
}

- (void)updateBuffer:(float*)data length:(int)length {
    
    // adding data to buffer
    [self addDataToBuffer:data ofLength:length];
    
    if (_delegate && [_delegate respondsToSelector:@selector(DidUpdateBuffer:OfLength:)]) {
        [self.delegate DidUpdateBuffer:data OfLength:length];
    }
    
    // adding data to energy buffer
    for(int i = 0; i < tempEnergyBufferLength; i++) {
        
        [self addEnergy:tempEnergyBuffer[i]];
    }
    
    // getting energy value
    
    NSDate* now = [NSDate date];
    NSTimeInterval interval = [now timeIntervalSinceDate:lastTimeEnergyValueUpdate];
    
    if (interval >= _energyUpdateTime) {
        lastTimeEnergyValueUpdate = now;
        float energy = [self getEnergy];
        if (_delegate && [_delegate respondsToSelector:@selector(DidGetEnergy:atTime:ofLength:)]) {
            [self.delegate DidGetEnergy:energy atTime:lastTimeEnergyValueUpdate ofLength:_energyBufferLength * FrameLength];
        }
    }
}

-(double) getEnergy {
    
    double energy = 0;
    
    for (int i = 0; i < _energyBufferLength; i++) {
        
        energy += _energyBuffer[i];
    }
    
    return energy;
}

// converting sound buffer from int values to float
// and computing energies of frame and also decibell frame energy

-(float*)convertBufferToDouble:(SInt16*)inBuffer withLength:(int) length {
    
    double frameEnergy = 0;

    float* convertedBuffer = (float*)malloc(sizeof(float) * length);
    
    int tempEnergyBufferPosition = 0;
    int frameCounter = 0;
    
    for (int i = 0; i < length; i++) {
        
        float f = (float)inBuffer[i] / 32768.0;
        
        convertedBuffer[i] = f;
        double fe = f;
        frameEnergy += fe*fe;
        
        if (frameCounter == FrameLength-1) {
            
            tempEnergyBuffer[tempEnergyBufferPosition] = frameEnergy;
            frameEnergy = 0;
            tempEnergyBufferPosition++;
            frameCounter = 0;
            
        } else frameCounter++;
        
    }
    
    free(inBuffer);
    
    return convertedBuffer;
}

-(void)computeBufferEnegry:(float*)inBuffer length:(int)length {
    
    double frameEnergy = 0;
    
    int tempEnergyBufferPosition = 0;
    int frameCounter = 0;
    
    for (int i = 0; i < length; i++)
    {
        double f = inBuffer[i];
        frameEnergy += f*f;
        
        if (frameCounter == FrameLength-1) {
            
            tempEnergyBuffer[tempEnergyBufferPosition++] = frameEnergy;
            frameEnergy = 0;
            frameCounter = 0;
            
        } else frameCounter++;
    }
}

-(void)dealloc {
    
    free(_buffer);
    free(_energyBuffer);
    free(tempEnergyBuffer);
}

@end
