//
//  HMMManager.m
//  BeWarned
//
//  Created by ivan verenich on 8/18/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import "HMMManager.h"
#import "HMM.h"
#import "DDXML.h"
#import "ModelConstants.h"

@interface HMMManager()


/**
 Represents hidden markov models
 */
@property (strong, nonatomic) NSMutableArray* models;

/**
 mean of every component of feature
 */
@property (assign, nonatomic) double* expectedValues;

/**
 Standart deviation of evert component of feature
 */
@property (assign, nonatomic) double* standartDeviation;

/**
 Clusterization manager
 */
@property (strong, nonatomic) id<ClusterizationManagerProtocol> clusterManager;

/**
 hmm files
 */
@property (nonatomic, strong) NSMutableArray* hmmFileNames;

@end

@implementation HMMManager

- (instancetype) initWithClusterizationManager:(id<ClusterizationManagerProtocol>) clusterManager {
    
    self = [super init];
    if (self) {
        
        _expectedValues = (double*)malloc(sizeof(double)*FeatureCoefficientCount);
        _standartDeviation = (double*)malloc(sizeof(double)*FeatureCoefficientCount);
        
        _clusterManager = clusterManager;
        
        NSString *filePath = [[NSBundle mainBundle]pathForResource:@"hmmManager" ofType:@"xml"];
        NSError* error = nil;
        NSString *xmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        DDXMLDocument *xmlDoc = [[DDXMLDocument alloc]initWithXMLString:xmlString options:0 error:&error];
        DDXMLElement *rootNode = [xmlDoc rootElement];
        
        [self deserializeFromXml:rootNode];
        
        _models = [NSMutableArray new];
        
        for (NSString* filename in _hmmFileNames) {
            
            NSArray<NSString*>* components = [filename componentsSeparatedByString:@"."];
            [_models addObject:[[HMM alloc] initFromFile:components[0]]];
        }
        
        rootNode = nil;
        xmlDoc = nil;
    }
    
    return self;
}

- (NSArray<NSNumber*>*) normalize:(double*)cepstralVector {
    
    NSMutableArray<NSNumber*>* normalized = [NSMutableArray new];
    
    for(int i = 0; i < FeatureCoefficientCount; i++) {
        
        double normalizedValue = (cepstralVector[i] - _expectedValues[i])/ _standartDeviation[i];
        [normalized addObject:@(normalizedValue)];
    }
    
    return normalized;
}

- (int*) getObservationByImage:(double**)image {
    
    int* observations = (int*)malloc(sizeof(int) * _observationLength);
    
    for (int i = 0; i < _observationLength; i++) {
        
        NSArray<NSNumber*>* normalized = [self normalize:image[i]];
        
        Pnt* pnt = [[Pnt alloc] initWithVector:normalized];
        observations[i] = [_clusterManager getClusterNumber:pnt];
        //free(normalized);
    }
    
    return observations;
}

- (void) deserializeFromXml:(DDXMLElement*) node {
    
    DDXMLElement* xmlParams = (DDXMLElement*)[node childAtIndex:0];
    
    DDXMLElement* xmlMeanCoefficients   = (DDXMLElement*)[xmlParams childAtIndex:0];
    DDXMLElement* xmlStdCoefficients    = (DDXMLElement*)[xmlParams childAtIndex:1];
    DDXMLElement* xmlHMMFilenames       = (DDXMLElement*)[node childAtIndex:1];
    
    for (int i = 0; i < FeatureCoefficientCount; i++) {
        
        DDXMLElement* xmlRow = (DDXMLElement*)[xmlMeanCoefficients childAtIndex:i];
        NSString* val = [xmlRow stringValue];
        val = [val stringByReplacingOccurrencesOfString:@"," withString:@"."];
        _expectedValues[i] = [val doubleValue];
        
        xmlRow = (DDXMLElement*)[xmlStdCoefficients childAtIndex:i];
        val = [xmlRow stringValue];
        val = [val stringByReplacingOccurrencesOfString:@"," withString:@"."];
        _standartDeviation[i] = [val doubleValue];
        
        xmlRow = nil;
        
    }
    
    _hmmFileNames = [NSMutableArray new];
    
    for (DDXMLElement* element in xmlHMMFilenames.children) {
        
        [_hmmFileNames addObject:[[element attributeForName:@"Filename"]stringValue]];
    }
}

-(NSUInteger) recognize:(double**) image {
    
    int* observations = [self getObservationByImage:image];
    double maxProbability = -2;
    NSUInteger ind = 0;
    for (HMM* model in _models) {
        
        double probability = [model evaluate:observations length:(int)_observationLength usingLogarithm:NO];
        
        if (probability > maxProbability) {
            
            maxProbability = probability;
            ind = model.soundClassIndex;
        }
    }
    
    free(observations);
    
    return ind;
}

@end
