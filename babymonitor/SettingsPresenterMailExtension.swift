//
//  SettingsPresenterMailExtension.swift
//  babymonitor
//
//  Created by ivan verenich on 1/4/17.
//  Copyright © 2017 beWarned. All rights reserved.
//

import Foundation
import MessageUI

extension SettingsPresenter: MFMailComposeViewControllerDelegate {
    
    func configuredMailComposeViewController(email: String, subject: String) -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([email])
        mailComposerVC.setSubject(subject)
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func initFeedbackEmail() {
        
        let email = interactor!.getContactEmail()
        let subject = interactor!.getContactSubject()
        
        let mailComposeViewController = self.configuredMailComposeViewController(email: email, subject: subject)
        if (!MFMailComposeViewController.canSendMail()) {
            
            let alert = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                          message: NSLocalizedString("feedbackCanNotSend", comment: ""),
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""),
                                          style: UIAlertActionStyle.cancel,
                                          handler: nil))
            
            self.view!.presentViewController(vc: alert)
            return
        }
        
        self.view!.presentViewController(vc: mailComposeViewController)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
}
