//
//  ModeSettingsViewController.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ModeSettingsViewController: UIViewController, ModeSettingsViewProtocol {
    
    @IBOutlet weak var modeDescriptionLabel: UILabel!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var sliderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var flashSwitch: UISwitch!
    @IBOutlet weak var vibrationSwitch: UISwitch!
    @IBOutlet weak var soundSwitch: UISwitch!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    var presenter: ModeSettingsPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        backButton.image = UIImage(named: "ArrowBack")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        switch presenter!.mode.notificationMode {
            case .Sensitive:
                
                if (presenter!.deviceDestination == .Parent) {
                    sliderView.isHidden = true
                    sliderViewHeightConstraint.constant = 0
                }
                
                self.title = NSLocalizedString("SensitiveMode", comment: "")
                self.modeDescriptionLabel.text = NSLocalizedString("SensitiveModeDescription", comment: "")
                self.slider.value = presenter!.mode.sensitivityValue!
            case .Medium:
                self.title = NSLocalizedString("MediumMode", comment: "")
                self.modeDescriptionLabel.text = NSLocalizedString("MediumModeDescription", comment: "")
                sliderView.isHidden = true
                sliderViewHeightConstraint.constant = 0
            case .OnlyCrying:
                self.title = NSLocalizedString("OnlyCryingMode", comment: "")
                self.modeDescriptionLabel.text = NSLocalizedString("OnlyScreamModeDescription", comment: "")
                sliderView.isHidden = true
                sliderViewHeightConstraint.constant = 0
        }
        
        flashSwitch.isOn = presenter!.mode.enableFlash
        vibrationSwitch.isOn = presenter!.mode.enableVibration
        soundSwitch.isOn = presenter!.mode.enableSound
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func flashValueChanged(_ sender: Any) {
        
        presenter!.mode.enableFlash = flashSwitch.isOn
    }
    
    @IBAction func vibrationValueChanged(_ sender: Any) {
        
        presenter!.mode.enableVibration = vibrationSwitch.isOn
    }
    
    @IBAction func soundValueChanged(_ sender: Any) {
    
        presenter!.mode.enableSound = soundSwitch.isOn
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        
        presenter!.mode.sensitivityValue = slider.value
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        presenter!.closeDialog()
    }
    
}
