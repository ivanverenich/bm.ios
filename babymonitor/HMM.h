//
//  HMM.h
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HMM : NSObject

@property (strong, nonatomic, readonly) NSString* soundClassName; // sound class name
@property (assign, nonatomic, readonly) int soundClassIndex;      // sound class index

- (instancetype) initFromFile:(NSString*) fileName;
- (double) evaluate:(int*)observations length:(int)length usingLogarithm:(BOOL)logarithm;

@end
