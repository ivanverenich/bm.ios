//
//  ShareManager.swift
//  babymonitor
//
//  Created by ivan verenich on 12/3/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ShareManager: NSObject {
    
    var analytics: AnalyticsProtocol?
    func share(viewForPresenting : UIView) -> UIActivityViewController? {
        
        let text: String = NSLocalizedString("shareText", comment: "")
        let link = ShareManager.getLinkOfAppStore()!
        if ShareManager.getLinkOfAppStore() != nil{
            let objectsToShare: [AnyObject] = [ text as AnyObject, link ]
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = viewForPresenting
            activityViewController.excludedActivityTypes = [  ]
            activityViewController.completionWithItemsHandler = { activity, success, items, error in
                if !success {
                    return
                }
                
                if (self.analytics != nil) {
                    
                    self.analytics!.sendEvent(event: AnalyticsEvents.ShareApp)
                }
            }
            
            return activityViewController
        }
        
        return nil
    }
    
    static func getLinkOfAppStore() -> NSURL? {
        let contentUrl = "itms://itunes.apple.com/us/app/apple-store/id1183383615?mt=8"
        
        return NSURL(string: contentUrl)
    }
}
