//
//  VoiceActivityDetection.m
//  BeWarned
//
//  Created by Admin on 26.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "SoundActivityDetection.h"
#import "ModelConstants.h"
#import "SoundTecnique.h"

@interface SoundActivityDetection()
{
    float minSampleEnergy;                              // минимальная энергия сэмпла (отрезок распознаваемой длины)
    float minFrameEnergy;                               // минмальная энергия кадра (512) отсчетов
    float* samplesEnergies;                             // энергии сэмрлов
    float* detectBuffer;                                // буфер, в котором происходит детекция
    double* energyBuffer;                               // буфер энергии (просто пересчитанный)
    int detectBufferLength;                             // длина буфера, в котором происходит детекция
    int energyBufferLength;                             // длина энергетического буфера
    BOOL isDetecting;                                   // признак того, что еще идет дектекия
    
    int samplesEnergiesPosition;                        // позиция заполнения буфера энегрии для детекции
    int currentPosition;                                // позиция, от которой идет детекция
    int L;                                              // количество фреймов
    int d;                                              // временная задержка
    int ov2;                                            // параметр при учитывании стандартного отклонения
    float maxEnergyPerSample;                           // максимальная энергия для распознаваемого участка
    float energyThreshold;
    
    int recognizeLengthInSamples;
    int recognizeFramesCount;
}

@property (nonatomic, assign) NSTimeInterval interval;

@end

@implementation SoundActivityDetection

-(instancetype)initWithBufferTimeInterval:(NSTimeInterval)interval {
    
    self = [super init];
    if (self) {
        
        _interval                       = interval;
        
        maxEnergyPerSample              = 2.0;
        minSampleEnergy                 = 0.05;
        _minimalEnegryCoefficient       = 0;
        minFrameEnergy                  = 3.0;
        ov2                             = 5;
        isDetecting = NO;
        detectBuffer = NULL;
        detectBufferLength = 0;
        
        L = 9;
        d = (L - 1) / 2;
        
        samplesEnergies = malloc(sizeof(float)*L);
        memset(samplesEnergies, 0, sizeof(float)*L);
        
        currentPosition = 0;
        
        recognizeLengthInSamples = [SoundTecnique RoundByFrameLength:[SoundTecnique ConvertToSamplesCountTime:interval]];
        recognizeFramesCount = recognizeLengthInSamples / FrameLength;
        
        [self reset];
    }
    return self;
}

/**
 Sets sensetivity coefficient

 @param minimalEnegryCoefficient minimal enegry coefficient
 */
- (void) setMinimalEnegryCoefficient:(float)minimalEnegryCoefficient {
    
    _minimalEnegryCoefficient = minimalEnegryCoefficient;
    energyThreshold = minSampleEnergy + _minimalEnegryCoefficient * maxEnergyPerSample / 100;
}

- (void) setSensetivityMode:(NSInteger)sensetivityMode {
    
    _sensetivityMode = sensetivityMode;
    if (_sensetivityMode == 0) {
        minSampleEnergy = 0.05;
        energyThreshold = minSampleEnergy + _minimalEnegryCoefficient * maxEnergyPerSample / 100;
    } else if (_sensetivityMode == 1) {
        minSampleEnergy = 5;
        energyThreshold = 10;
    } else {
        minSampleEnergy = 25;
        energyThreshold = 50;
    }
}

/**
 Comparator

 @param a first value
 @param b second value

 @return compare result
 */
int compare (const void * a, const void * b) {
    float fa = *(const float*) a;
    float fb = *(const float*) b;
    return (fa > fb) - (fa < fb);
}

/**
 Median operator. Sorts array of energies asc and returns energy of central sample in array.

 @return energy of center sample
 */
- (float) MED {
    
    float* temp =  malloc(sizeof(float)*L);
    memcpy(temp, samplesEnergies, sizeof(float)*L);
    qsort(temp, L, sizeof(float), compare);
    float medV = temp[d];
    free(temp);
    
    return medV;
}

/**
 Gets standard deviation between middle and array of energies

 @param middle middle value

 @return standard deviation
 */
- (float) std:(float)middle {
    
    float sum = 0;
    
    for (int i = 0; i < L; i++) {
        
        float v = samplesEnergies[i] - middle;
        sum += v * v;
    }
    
    return sqrtf(sum / L);
}

/**
 Pushes energy value to array of energies. Pushes array to left when overflow

 @param energy energy value
 */
- (void) pushToSamplesEnergies:(float) energy {
  
    if (samplesEnergiesPosition < L) {
        
        samplesEnergies[samplesEnergiesPosition] = energy;
        samplesEnergiesPosition++;
        
    } else {
        
        memmove(samplesEnergies, &samplesEnergies[1], sizeof(float)* (L-1));
        samplesEnergies[L-1] = energy;
    }
}

/**
 Checks that detection can be started from current energy value

 @param energy energy value

 @return true, if detection can be started from current energy value
 */
- (BOOL) isTheBeginningOfDetection:(float) energy {
  
//    if (energy < minFrameEnergy) return NO;
//    
//    float med = [self MED];
//    if (energy > ov2 * [self std:med] + med) return YES;
//    
//    return NO;
    
    float avarageEnergy = 0;
    for (int i = 0; i < L; i++) {
        avarageEnergy+= samplesEnergies[i];
    }
    avarageEnergy = avarageEnergy / L;
    
    if (energy > avarageEnergy * 3) {
        return YES;
    }
    
    return NO;
    
}

/**
 Checks is array of energies full

 @return true, if array is full
 */
- (BOOL) isSamplesEnergyFull {
    
    if (samplesEnergiesPosition < L) return NO;
    return YES;
}

/**
 Resets detection position
 */
- (void) reset {
    
    samplesEnergiesPosition = 0;
}

/**
 Clears detection array and correspond resources
 */
-(void) clear {
    
    //memset(detectBuffer, 0, sizeof(float) * detectBufferLength);
    //memset(energyBuffer, 0, sizeof(float) * energyBufferLength);
}

/**
 Detects impulsive potentially dangerous sound in buffer
 */
-(void) detectionImpulsiveAlgorithm {
    
    int curPos = 0;
    
    while (curPos < energyBufferLength - (recognizeFramesCount + 1)) {
        
        double frameEnegry = energyBuffer[curPos];
        
        if (![self isSamplesEnergyFull]) {
            [self pushToSamplesEnergies:frameEnegry];
            curPos++;
            continue;
        }
        
        if (![self isTheBeginningOfDetection:frameEnegry]) {
            [self pushToSamplesEnergies:frameEnegry];
            curPos++;
            continue;
        }
        
        double sampleEnergyFirstPart = 0;
        double sampleEnergySecondPart = 0;
        int j = curPos;
        int framesCount = 0;
        while (j < energyBufferLength && framesCount < recognizeFramesCount) {
                    
            if (framesCount < recognizeFramesCount / 2) {
                sampleEnergyFirstPart += energyBuffer[j];
            } else {
                sampleEnergySecondPart += energyBuffer[j];
            }
                    
            j++;
            framesCount++;
        }
        
        //NSLog(@"%f", sampleEnergyFirstPart + sampleEnergySecondPart);
                
        if ( (sampleEnergyFirstPart + sampleEnergySecondPart) >= energyThreshold && sampleEnergyFirstPart > minSampleEnergy / 2 &&
            sampleEnergySecondPart > minSampleEnergy / 2) {
                    
            int startPtr = curPos * FrameLength;
            int endPtr = (curPos + recognizeFramesCount + 1) * FrameLength;
            endPtr = min(endPtr, detectBufferLength);
            int elemCount = endPtr - startPtr;
                    
            float* bufferForHandling = malloc(sizeof(float)*elemCount);
            memcpy(bufferForHandling, &detectBuffer[startPtr], sizeof(float)*elemCount);
                    
            free(energyBuffer);
            free(detectBuffer);
                    
            isDetecting = NO;
            
            if (_delegate && [_delegate respondsToSelector:@selector(didDetected:ofLength:)]) {
                [self.delegate didDetected:bufferForHandling ofLength:elemCount];
            }
            break;
        }
        
        [self pushToSamplesEnergies:frameEnegry];
        curPos++;
    }
    
    if (isDetecting) {

        free(energyBuffer);
        free(detectBuffer);
        
        isDetecting = NO;
        [self.delegate didNotDetected];
    }
}

-(void) detectionSensitiveAlgorithm {
    
    int curPos = 0;
    double maxEnergy = 0;
    int maxEnergyPos = 0;
    
    while (curPos < energyBufferLength - (recognizeFramesCount + 1)) {
        
        int i = curPos;
        double energy =  0;
        while (i < recognizeFramesCount) {
            energy += energyBuffer[i];
            i++;
        }
        
        if (energy > maxEnergy) {
            maxEnergy = energy;
            maxEnergyPos = curPos;
        }
        
        curPos++;
    }
    
    NSLog(@"%f", maxEnergy);
    
    if ( maxEnergy >= energyThreshold) {
        
        int startPtr = maxEnergyPos * FrameLength;
        int endPtr = (maxEnergyPos + recognizeFramesCount + 1) * FrameLength;
        endPtr = min(endPtr, detectBufferLength);
        int elemCount = endPtr - startPtr;
        
        float* bufferForHandling = malloc(sizeof(float)*elemCount);
        memcpy(bufferForHandling, &detectBuffer[startPtr], sizeof(float)*elemCount);
        
        free(energyBuffer);
        free(detectBuffer);
        
        isDetecting = NO;
        
        if (_delegate && [_delegate respondsToSelector:@selector(didDetected:ofLength:)]) {
            [self.delegate didDetected:bufferForHandling ofLength:elemCount];
        }
        return;
    }
    
    if (isDetecting) {
        
        free(energyBuffer);
        free(detectBuffer);
        
        isDetecting = NO;
        [self.delegate didNotDetected];
    }
}

/**
 User function for detection in buffer

 @param inBuffer             in buffer
 @param length               in buffer length
 @param inEnergyBuffer       in energy buffer
 @param inEnergyBufferLength in energy buffer length
 */
-(void)detectBuffer:(float*)inBuffer ofLength:(int)length energyBuffer:(double*)inEnergyBuffer ofLength:(int)inEnergyBufferLength {
 
    // if inpossible to perform detection, than just free allocated memory
    if (isDetecting) {
        
        free(inBuffer);
        free(inEnergyBuffer);
    }
    else {
        
        isDetecting = YES;
        
        // copy data in local buffer and free the memory

        // data buffer
        detectBuffer = (float*)malloc(sizeof(float)*length);
        memcpy(detectBuffer, inBuffer, sizeof(float)*length);
        detectBufferLength = length;
        free(inBuffer);
        
        // energy buffer
        energyBuffer = (double*)malloc(sizeof(double)*inEnergyBufferLength);
        memcpy(energyBuffer, inEnergyBuffer, sizeof(double)*inEnergyBufferLength);
        energyBufferLength = inEnergyBufferLength;
        free(inEnergyBuffer);
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            if (_sensetivityMode != 0) {
                [self detectionImpulsiveAlgorithm];
            } else {
                [self detectionSensitiveAlgorithm];
            }
            
        });
    }
}

@end
