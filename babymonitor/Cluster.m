//
//  Cluster.m
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import "Cluster.h"

@implementation Cluster

- (instancetype) initWithNumber:(int)number centroid:(Pnt*)centerPoint {
    
    self = [super init];
    
    if (self) {
    
        _number = number;
        _centroid = centerPoint;
    }
    
    return self;
}

@end
