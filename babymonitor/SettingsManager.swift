//
//  SettingsManager.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

let defaultsSettingsKey = "appSettings"
let observerRemoteEnabilityIdentifier = "settingsEnableRemoteChanged"
let observerVideoEnabilityIdentifier = "settingsEnableVideoChanged"

class SettingsManager: NSObject, NSCoding {

    private var appIsFree: Bool = true
    var enableRemote: Bool = true {
        didSet {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:observerRemoteEnabilityIdentifier),
                                            object: NSNumber.init(value: enableRemote))
        }
    }
    var enableVideo: Bool = true {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:observerVideoEnabilityIdentifier),
                                            object: NSNumber.init(value: enableVideo))
        }
    }
    var currentMode: NotificationMode = NotificationMode.Sensitive
    var sensitiveModeSettings: ModeSettingsDTO?
    var mediumModeSettings: ModeSettingsDTO?
    var onlyCryingModeSettings: ModeSettingsDTO?
    var deviceDestination: DeviceDestination = DeviceDestination.NotDefined
    var isMotionDetectOn: Bool = true
    var isFree: Bool {
        get { return appIsFree }
    }
    
    private override init() {
        
        sensitiveModeSettings = ModeSettingsDTO.init(flash: true, vibration: true, sound: true, mode: .Sensitive)
        sensitiveModeSettings?.sensitivityValue = 50
        
        mediumModeSettings = ModeSettingsDTO.init(flash: true, vibration: true, sound: true, mode: .Medium)
        onlyCryingModeSettings = ModeSettingsDTO.init(flash: true, vibration: true, sound: true, mode: .OnlyCrying)
    }
    
    required convenience init?(coder decoder: NSCoder) {
        
        self.init()
        appIsFree = decoder.decodeBool(forKey: "appIsFree")
        deviceDestination = DeviceDestination(rawValue:decoder.decodeInteger(forKey: "MS_deviceDestination"))!
        currentMode = NotificationMode(rawValue: decoder.decodeInteger(forKey: "MS_currentMode"))!
        enableRemote = decoder.decodeBool(forKey: "enableRemote")
        enableVideo = decoder.decodeBool(forKey: "enableVideo")
        isMotionDetectOn = decoder.decodeBool(forKey: "isMotionDetectOn")
        sensitiveModeSettings = decoder.decodeObject(forKey: "sensitiveModeSettings") as? ModeSettingsDTO
        mediumModeSettings = decoder.decodeObject(forKey: "mediumModeSettings") as? ModeSettingsDTO
        onlyCryingModeSettings = decoder.decodeObject(forKey: "onlyCryingModeSettings") as? ModeSettingsDTO
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(appIsFree, forKey: "appIsFree")
        aCoder.encode(deviceDestination.rawValue, forKey: "MS_deviceDestination")
        aCoder.encode(currentMode.rawValue, forKey: "MS_currentMode")
        aCoder.encode(enableRemote, forKey: "enableRemote")
        aCoder.encode(enableVideo, forKey: "enableVideo")
        aCoder.encode(isMotionDetectOn, forKey: "isMotionDetectOn")
        aCoder.encode(sensitiveModeSettings, forKey: "sensitiveModeSettings")
        aCoder.encode(mediumModeSettings, forKey: "mediumModeSettings")
        aCoder.encode(onlyCryingModeSettings, forKey: "onlyCryingModeSettings")
    }
    
    class func loadSettings() -> SettingsManager {
        
        let defaults = UserDefaults.standard
        
        if let savedSettings = defaults.object(forKey: defaultsSettingsKey) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: savedSettings) as! SettingsManager
        }
        
        return SettingsManager()
    }
    
    func save() {
        
        let savedData = NSKeyedArchiver.archivedData(withRootObject: self)
        let defaults = UserDefaults.standard
        defaults.set(savedData, forKey: defaultsSettingsKey)
        defaults.synchronize()
    }
    
    func setAppIsPaid() {
        
        self.appIsFree = false
    }
}
