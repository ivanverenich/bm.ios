//
//  ParentWireframe.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ParentWireframe: BaseWireFrame, ParentWireframeProtocol {

    func showSettingsDialogs(settings: SettingsManager) {
        
        let settingsVC = iphoneSettingsControllerFromStoryboard()
        
        let wireFrame = SettingsWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = SettingsInteractor()
        interactor.settings = settings
        interactor.analytics = Analytics()
        
        let presenter = SettingsPresenter()
        presenter.deviceDestation = .Parent
        settingsVC.presenter = presenter
        presenter.view = settingsVC
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    func iphoneSettingsControllerFromStoryboard() -> IphoneSettingsViewController {
        
        let storyboard = MainWireframe.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: iphoneSettingsViewControllerIdentifier) as! IphoneSettingsViewController
        
        return vc
    }
}
