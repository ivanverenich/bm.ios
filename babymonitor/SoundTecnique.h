//
//  SoundTecnique.h
//  BeWarned
//
//  Created by Admin on 25.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//
//  Вспомогательыне функции для преобразований

#import <Foundation/Foundation.h>

// return max value for given values
#define max(a, b) (((a) > (b)) ? (a) : (b))
// return min value for given values
#define min(a, b) (((a) < (b)) ? (a) : (b))

@interface SoundTecnique : NSObject
    // конвертация времени в количество отсчетов
    +(int)ConvertToSamplesCountTime:(NSTimeInterval)timeInterval;

    // округление количества отсчетов до длины фрейма
    // количество сэмплов в фрейме (длина фрейма) из констант приложения
    +(int)RoundByFrameLength:(int)SamplesCount;
@end
