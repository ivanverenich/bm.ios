//
//  HMM.m
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import "HMM.h"
#import "DDXML.h"

@interface HMM()


@property (assign, nonatomic) int N;                    // number of hidden states
@property (assign, nonatomic) int M;                    // vocabulary size
@property (assign, nonatomic) double* pi;               // vector of initial states
@property (assign, nonatomic) double** A;               // matrix of probabilities of transition states
@property (assign, nonatomic) double** B;               // matrix of probabilities of observation k-th symbol in i state

@end

@implementation HMM

- (instancetype) initFromFile:(NSString*) fileName {
    
    self = [super init];
    if (self) {
        
        NSString *filePath = [[NSBundle mainBundle]pathForResource:fileName ofType:@"xml"];
        NSError* error = nil;
        NSString *xmlString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        DDXMLDocument *xmlDoc = [[DDXMLDocument alloc]initWithXMLString:xmlString options:0 error:&error];
        DDXMLElement *rootNode = [xmlDoc rootElement];
        
        [self deserializeFromXml:rootNode];
        
        rootNode = nil;
        xmlDoc = nil;
        
    }
    
    return self;
    
}

- (void) deserializeFromXml:(DDXMLElement*) node {
    
    _N                  = [[[node attributeForName:@"StateCount"]stringValue] intValue];
    _M                  = [[[node attributeForName:@"VocabularySize"]stringValue] intValue];
    _soundClassName     = [[node attributeForName:@"SoundClassName"]stringValue];
    _soundClassIndex    = [[[node attributeForName:@"SoundClassIndex"]stringValue] intValue];
    
    _pi                 = (double*)malloc(sizeof(double) * _N);
    _A                  = malloc(sizeof(double*) * _N);
    
    for (int k = 0; k < _N; k++) {
        _A[k] = malloc(sizeof(double) * _N);
    }
    
    _B                  = malloc(sizeof(double*) * _N);
    
    for (int k = 0; k < _N; k++) {
        _B[k] = malloc(sizeof(double) * _M);
    }
    
    DDXMLElement* xmlPiArray                            = (DDXMLElement*)[node childAtIndex:0];
    DDXMLElement* xmlTransitionMatrix                   = (DDXMLElement*)[node childAtIndex:1];
    DDXMLElement* xmlObservationProbabilitiesMatrix     = (DDXMLElement*)[node childAtIndex:2];

    int index = 0;
    for (DDXMLElement* xmlRow in xmlPiArray.children) {
        
        NSString* val = [[xmlRow attributeForName:@"value"]stringValue];
        val = [val stringByReplacingOccurrencesOfString:@"," withString:@"."];
        _pi[index++] = [val doubleValue];
        
    }
    
    for (DDXMLElement* xmlRow in xmlTransitionMatrix.children) {
        
        int row         = [[[xmlRow attributeForName:@"row"] stringValue] intValue];
        int column      = [[[xmlRow attributeForName:@"column"] stringValue] intValue];
        
        NSString* val = [[xmlRow attributeForName:@"value"] stringValue];
        val = [val stringByReplacingOccurrencesOfString:@"," withString:@"."];
        
        _A[row][column] = [val doubleValue];
        
    }
    
    for (DDXMLElement* xmlRow in xmlObservationProbabilitiesMatrix.children) {
        
        int row         = [[[xmlRow attributeForName:@"row"] stringValue] intValue];
        int column      = [[[xmlRow attributeForName:@"column"] stringValue] intValue];
        
        NSString* val = [[xmlRow attributeForName:@"value"] stringValue];
        val = [val stringByReplacingOccurrencesOfString:@"," withString:@"."];
        
        _B[row][column] = [val doubleValue];
        
    }
    
}

- (double**) forward:(int*) observations length:(int)observationLength outCoefficients:(double*) c {
    
    int T = observationLength;
    
    double** fwd = malloc(sizeof(double*) * T);
    for (int k = 0; k < T; k++) {
        fwd[k] = malloc(sizeof(double) * _N);
        memset(fwd[k], 0, sizeof(double) * _N);
    }
    
    // 1. Initialization
    for (int i = 0; i < _N; i++) {
        
        fwd[0][i] = _pi[i] * _B[i][ observations[0] ];
        c[0] += fwd[0][i];
    }
    
    if (c[0] != 0) {    // scaling
        
        for (int i = 0; i < _N; i++) fwd[0][i] = fwd[0][i] / c[0];
        
    }
    
    // 2. Induction
    
    for (int t = 1; t < T; t++) {
        
        for (int i = 0; i < _N; i++) {
            
            double p = _B[i][observations[t]];
            
            double sum = 0.;
            
            for (int j = 0; j < _N; j++) sum += fwd[t-1][j] * _A[j][i];
            
            fwd[t][i] = sum * p;
                
            c[t] += fwd[t][i]; // scaling coefficient
            
        }
        
        if (c[t] != 0) {
            
            for (int i = 0; i < _N; i++) fwd[t][i] = fwd[t][i] / c[t];
            
        }
        
    }
    
    
    return fwd;
    
}

- (double) evaluate:(int*)observations length:(int)length usingLogarithm:(BOOL)logarithm {
    
    if (!observations) return 0.;
    if (length == 0) return 0.;
    
    // forward algorithm'
    double likelihood = 0.;
    double* coefficients = (double*) malloc(sizeof(double) * length);
    memset(coefficients, 0, sizeof(double) * length);
    
    //
    
    [self forward:observations length:length outCoefficients:coefficients];
    
    for (int i = 0; i < length; i++) likelihood += log(coefficients[i]);
    
    return logarithm ? likelihood : exp(likelihood);
    
}

@end
