//
//  SoundNotifier.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import AVFoundation

class SoundNotifier: NSObject, NotoficatorProtocol {
    
    private var player: AVAudioPlayer?
    
    private func setupAudioSession() {
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
            try session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
            try session.setActive(true)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func performNotification() {
        
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: "notification_sound", ofType: "wav")!)
        
        do {
            setupAudioSession()
            player = try AVAudioPlayer(contentsOf: sound)
            player!.volume = 1.0
            player!.numberOfLoops = 0
            player!.prepareToPlay()
            player!.play()
            
            //print("played")
        } catch {
            
        }
    }

    func notify() {
        
        //DispatchQueue.main.async {
            self.performNotification()
        //}
    }
}

