//
//  Clusterization.h
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pnt.h"

@protocol ClusterizationManagerProtocol <NSObject>

@required
    - (int) getClusterNumber:(Pnt*) point;

@end

/**
 Represents clusterizator
 */
@interface ClusterizationManager : NSObject <ClusterizationManagerProtocol>


/**
 Constructor from file

 @param filename file name
 @return object
 */
-(instancetype)initWithFile:(NSString*)filename;


/**
 Gets number of cluster, which center is the nearest to the point

 @param point point object
 @return cluster number
 */
- (int) getClusterNumber:(Pnt*) point;

@end
