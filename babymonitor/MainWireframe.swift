//
//  MainWireframe.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

let iphoneSettingsViewControllerIdentifier = "settings"

class MainWireframe: BaseWireFrame, MainWireframeProtocol {
    
    func presentSharingDialog(presenterVC: UIViewController, sharingVC: UIActivityViewController?) {
        
        presenterVC.present(sharingVC!, animated: true, completion: nil)
    }

    func openSettingsDialog(settings: SettingsManager) {
        
        let settingsVC = iphoneSettingsControllerFromStoryboard()
        
        let wireFrame = SettingsWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = SettingsInteractor()
        interactor.settings = settings
        interactor.analytics = Analytics()
        
        let presenter = SettingsPresenter()
        presenter.deviceDestation = .Child
        settingsVC.presenter = presenter
        presenter.view = settingsVC
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    func iphoneSettingsControllerFromStoryboard() -> IphoneSettingsViewController {
        
        let storyboard = MainWireframe.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: iphoneSettingsViewControllerIdentifier) as! IphoneSettingsViewController
        
        return vc
    }
}
