//
//  Cluster.h
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pnt.h"

/**
 <#Description#>
 */
@interface Cluster : NSObject

- (instancetype) initWithNumber:(int)number centroid:(Pnt*)centerPoint;
@property (assign, nonatomic, readonly) int number;
@property (strong, nonatomic, readonly) Pnt* centroid;

@end
