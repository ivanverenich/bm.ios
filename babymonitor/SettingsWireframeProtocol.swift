//
//  SettingsWireframeProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol SettingsWireframeProtocol {
    
    func showPeersDialog()
    func showModeSettingsDialog(mode: ModeSettingsDTO, deviceDestination: DeviceDestination)
    func closeDialog()
}
