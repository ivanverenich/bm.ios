//
//  ParentMainViewController.swift
//  babymonitor
//
//  Created by ivan verenich on 12/2/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ParentMainViewController: UIViewController, ParentViewProtocol {

    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var informationIV: UIImageView!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var playImage: UIImageView!
    
    // trial info
    
    @IBOutlet weak var trialView: UIView!
    @IBOutlet weak var trialInfoLabel: UILabel!
    @IBOutlet weak var trialTimerLabel: UILabel!
    
    let imageConnected = UIImage.init(named: "connected_icon")
    let imageAttention = UIImage.init(named: "attention_icon")
    let imageSilence   = UIImage.init(named: "silence_icon")
    
    var presenter: ParentPresenterProtocol?
    var motionView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsButton.image = UIImage(named: "settings_icon")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        self.informationLabel.text = NSLocalizedString("statusLostAllConnections", comment: "")
        self.informationLabel.textColor = errorColor
        informationIV.image = imageAttention
        
        videoPreview.layer.borderWidth = 6.0
        videoPreview.layer.borderColor = borderColor.cgColor
        
        trialView.isHidden = true
        
        setupMotionView()
    }
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        
        presenter!.showSettingsDialogs()
    }
    
    // MARK: ParentViewProtocol
    
    func updateStatusConnectionLost() {
     
        DispatchQueue.main.async {
            self.informationLabel.text = NSLocalizedString("statusLostAllConnections", comment: "")
            self.informationLabel.textColor = errorColor
            self.informationIV.image = self.imageAttention
            
            self.playImage.isHidden = false
            self.imageView.image = nil
        }
    }
    
    func updateStatusConnectionEstablished() {
        
        DispatchQueue.main.async {
            self.informationLabel.text = NSLocalizedString("statusEstablishedConnections", comment: "")
            self.informationLabel.textColor = okColor
            self.informationIV.image = self.imageConnected
        }
    }
    
    func updateRemoteVideoImage(image: UIImage, motionRect: CGRect?) {
        
        DispatchQueue.main.async {
            
            if !self.playImage.isHidden {
                self.playImage.isHidden = true
            }
            
            self.imageView.image = image
            
            if let rect = motionRect {
                
                //print("Motion source rect = \(rect)")
            
                let imageSize = CGSize(width: image.size.width * image.scale, height: image.size.height * image.scale)
                let tramsformedRect = self.getMotionViewFrame(imageSize, self.imageView.frame.size, motionRect: rect)
                
                //print("Motion tramsformed rect = \(tramsformedRect)")
                
                self.motionView.frame = tramsformedRect
                if !self.motionView.isDescendant(of: self.imageView) {
                    self.imageView.addSubview(self.motionView)
                }
            } else {
                if self.motionView.isDescendant(of: self.imageView) {
                    self.motionView.removeFromSuperview()
                }
            }
        }
    }
    
    func stopedReceiveingRemoteImage() {
        
        DispatchQueue.main.async {
            
            self.playImage.isHidden = false
            self.imageView.image = nil
        }
    }
    
    func didReceiveRemoteEvent(event: NotificationEvent)  {
        
        DispatchQueue.main.async {
            
            switch event {
            case .noise:
                self.informationLabel.text = NSLocalizedString("statusNoiseNotification", comment: "")
                self.informationLabel.textColor = errorColor
                self.informationIV.image = self.imageAttention
            case .childWhining:
                self.informationLabel.text = NSLocalizedString("statusWhinningNotification", comment: "")
                self.informationLabel.textColor = errorColor
                self.informationIV.image = self.imageAttention
            case .silence:
                self.informationLabel.text = NSLocalizedString("statusSilenceNotification", comment: "")
                self.informationLabel.textColor = okColor
                self.informationIV.image = self.imageSilence
            case .motion:
                self.informationLabel.text = NSLocalizedString("statusMotionNotification", comment: "")
                self.informationLabel.textColor = errorColor
                self.informationIV.image = self.imageAttention
            default:
                self.informationLabel.text = NSLocalizedString("statusCryingNotification", comment: "")
                self.informationLabel.textColor = errorColor
                self.informationIV.image = self.imageAttention
            }
        }
    }
    
    func didReceiveTrialCounter(timeLeft: Int) {
        
        DispatchQueue.main.async {
        
            if (self.trialView.isHidden) {
                self.trialView.isHidden = false
            }
        
            let minutes = timeLeft / 60
            let seconds = timeLeft - minutes * 60
        
            let minutesString = String.init(format: "%02d", minutes)
            let secondsString = String.init(format: "%02d", seconds)
        
            self.trialTimerLabel.text = "\(minutesString):\(secondsString)"
        }
    }
    
    // MARK: Private methods
    
    private func setupMotionView() {
        
        self.motionView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        self.motionView.backgroundColor = UIColor.clear
        self.motionView.layer.borderWidth = 1
        self.motionView.layer.borderColor = UIColor.red.cgColor
    }
    
    private func getMotionViewFrame(_ worldSize: CGSize, _ viewport: CGSize, motionRect: CGRect) -> CGRect {
        
        let sX: CGFloat = viewport.width / worldSize.width
        let sY: CGFloat = viewport.height / worldSize.height
        
        let tX: CGFloat = 0
        let tY: CGFloat = 0
        
        var x = motionRect.origin.x * sX + tX
        var y = motionRect.origin.y * sY + tY
        var width = motionRect.size.width * sX + tX
        var height = motionRect.size.height * sY + tY
        
        if (x < 0) { x = 0 }
        if (y < 0) { y = 0 }
        if (width + x > viewport.width) { width = viewport.width - x }
        if (height + y > viewport.height) { height = viewport.height - y }
        
        return CGRect.init(x: x, y: y, width: width, height: height)
    }
}
