//
//  Analytics.swift
//  babymonitor
//
//  Created by ivan verenich on 12/13/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import Firebase

protocol AnalyticsProtocol {
    func sendEvent(event: AnalyticsEvents)
}

class Analytics: NSObject, AnalyticsProtocol {

    class func setup() {
        
        FIRApp.configure()
    }
    
    func sendEvent(event: AnalyticsEvents) {
        
        FIRAnalytics.logEvent(withName: kFIREventSelectContent, parameters: [
            kFIRParameterContentType : event.rawValue as NSObject])
    }
}
