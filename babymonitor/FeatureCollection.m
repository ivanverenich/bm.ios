//
//  FeatureCollection.m
//  BeWarned
//
//  Created by Admin on 01.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//
//  описывает коллекцию признаков на одном звуковом образце

#import "FeatureCollection.h"
#import "babymonitor-Swift.h"


@interface FeatureCollection()

@property (nonatomic, strong) NSString* className;
@property (nonatomic, assign) BOOL isNoisy;
@property (nonatomic, strong) NSMutableArray<id<FeatureFrameProtocol>>* featureFrames;

@end

@implementation FeatureCollection

// конструктор по отрезку звуковой волны
//
-(id)initWithBuffer:(float*) buffer OfLength:(int)length frameDuration:(int)frameDuration frameShift:(int)frameShift {
    
    self = [super init];
    if (self) {
        
        float* buf = (float*)malloc(sizeof(float) * frameDuration);
        _featureFrames = [NSMutableArray new];
        long startIndex = 0;
        
        while (startIndex + frameDuration < length) {
            
            memcpy(buf, &buffer[startIndex], sizeof(float) * frameDuration);
            MfccFrame *frame = [[MfccFrame alloc] initWithPointer:buf length:frameDuration];
            [_featureFrames addObject:frame];
            startIndex += frameShift;
        }
        
        free(buf);
    }
    
    return self;
}

-(double**) getFeatureImage {
    
    int sizeX = (int)_featureFrames.count;
    int sizeY = FeatureCoefficientCount;
        
    double** image = (double**)malloc(sizeof(double*) * sizeX);
    
//    double sumOfEnergies0 = 0;
//    double sumOfEnergies1 = 0;
//    double sumOfEnergies2 = 0;
//    double sumOfEnergies3 = 0;
    
    _isNoisy = NO;
    
    for (int x = 0; x < sizeX; x++) {
        
        image[x] = (double*)malloc(sizeof(double) * sizeY);
        MfccFrame* currentFrame = [_featureFrames objectAtIndex:x];
        
        NSArray<NSNumber*>* CC = [currentFrame getFeatureCoefficietns];
            
        for (int y = 0; y < sizeY; y++)
        {
            image[x][y] = [CC[y] doubleValue];
        }
        
//        CepstrumFrame* cf = (CepstrumFrame*) currentFrame;
//        
//        //NSLog(@"%f", cf.powerOfSpectr[0]);
//        sumOfEnergies0 += cf.powerOfSpectr[0];
//        sumOfEnergies1 += cf.powerOfSpectr[1];
//        sumOfEnergies2 += cf.powerOfSpectr[2];
//        sumOfEnergies3 += cf.powerOfSpectr[3];
    }
    
//    // temporary turn off this feature
//    if (sumOfEnergies0 > minimalNoiseLevel) _isNoisy = YES;
//    
//    //NSLog(@"0..512 = %f, 512..1024 = %f, 1024..1536 = %f, 1536..2048 = %f", sumOfEnergies0, sumOfEnergies1, sumOfEnergies2, sumOfEnergies3);
    
    return image;
}
    
@end
