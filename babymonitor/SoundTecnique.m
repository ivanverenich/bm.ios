//
//  SoundTecnique.m
//  BeWarned
//
//  Created by Admin on 25.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "SoundTecnique.h"
#import "ModelConstants.h"
#import <math.h>

@implementation SoundTecnique

+(int)ConvertToSamplesCountTime:(NSTimeInterval)timeInterval {
    
    return trunc((double)SampleRate * timeInterval);
}

+(int)RoundByFrameLength:(int)SamplesCount
{
    double left = (double)SamplesCount / (double)FrameLength;
    double whole = trunc(left);
    if ((left - whole) > 0.5) return (int)(whole + 1) * FrameLength;
    
    return (int)whole * FrameLength;
}

@end
