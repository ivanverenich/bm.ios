//
//  ModeSettingsPresenter.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ModeSettingsPresenter: NSObject, ModeSettingsPresenterProtocol {

    var mode: ModeSettingsDTO
    var view: ModeSettingsViewProtocol?
    var wireframe: ModeSettingsWireframeProtocol?
    var interactor: ModeSettingsInteractorProtocol?
    var deviceDestination : DeviceDestination!
    
    init(notificationMode: ModeSettingsDTO) {
        
        mode = notificationMode
        super.init()
    }
    
    func closeDialog() {
        self.wireframe!.closeDialog()
    }
}
