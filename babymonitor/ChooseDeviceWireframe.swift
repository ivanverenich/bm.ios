//
//  ChooseDeviceWireframe.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ChooseDeviceWireframe: BaseWireFrame, ChooseDeviceWireframeProtocol {
    
    func presentParentDialog(settings: SettingsManager) {
        
        let vc = parentViewControllerFromStoryboard()
        
        let wireFrame = ParentWireframe()
        wireFrame.navigationController = navigationController
        
        let interactor = ParentInteractor()
        interactor.settings = settings
        
        let notificationService = NotificationService()
        notificationService.flashNotifier = FlashNotifier()
        notificationService.vibroNotifier = VibroNotifier()
        notificationService.soundNotifier = SoundNotifier()
        notificationService.settings = settings
        interactor.notificationService = notificationService
        
        let presenter = ParentPresenter()
        vc.presenter = presenter
        interactor.delegate = presenter
        
        presenter.view = vc
        presenter.wireframe = wireFrame
        presenter.interactor = interactor
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentChildDialog(settings: SettingsManager) {
        
        let vc = mainControllerFromStoryboard()
        
        let mainWireFrame = MainWireframe()
        mainWireFrame.navigationController = navigationController
        
        let interactor = MainInteractor()
        if (!UIDevice.current.isSimulator) {
            //let videoOutput = CustomCaptureOutput()
            //interactor.videoCapture = VideoCapture(captureOutput: videoOutput)
            interactor.videoCapture = VideoCaptureWithMotionDetect()
        }
        
        let detectionService = DetectionService()
        interactor.detectionService = detectionService
        
        let notificationService = NotificationService()
        notificationService.flashNotifier = FlashNotifier()
        notificationService.vibroNotifier = VibroNotifier()
        notificationService.soundNotifier = SoundNotifier()
        notificationService.settings = settings
        interactor.notificationService = notificationService
        interactor.settings = settings
        interactor.analytics = Analytics()
        
        // sound monitor
        let audioProcessor = AudioProcessor()
        
        let filePath = Bundle.main.path(forResource: "clusterizaztion", ofType: "xml")
        let clusterManager = ClusterizationManager.init(file: filePath)
        
        let hmmManager = HMMManager.init(clusterizationManager: clusterManager)
        hmmManager!.observationLength = UInt(FrameCountInSound)
        
        let recognitionManager = SoundRecognitionManager.init(recognazer: hmmManager)
        let soundActivityDetector = SoundActivityDetection.init(bufferTimeInterval: RecognizeLengthSec)
        
        let soundBuffer = SoundBuffer.init(bufferLength: BufferLength, updateLength: MicrophoneBufSize, energyUpdateTime: EnegryUpdateTime)
        
        let soundMonitor = SoundMonitorFlowControl(audioProcessor: audioProcessor,
                                                   recognitionManager: recognitionManager, soundActivityDetectionManager: soundActivityDetector, soundBuffer: soundBuffer)
        
        detectionService.soundMonitor = soundMonitor
        
        let mainPresenter = MainPresenter()
        vc.presenter = mainPresenter
        mainPresenter.view = vc
        mainPresenter.wireframe = mainWireFrame
        mainPresenter.interactor = interactor
        
        interactor.delegate = mainPresenter
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func mainControllerFromStoryboard() -> ViewController {
        
        let storyboard = LoadingWireFrame.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: mainControllerIdentifier) as! ViewController
        return vc
    }
    
    func parentViewControllerFromStoryboard() -> ParentMainViewController {
        
        let storyboard = LoadingWireFrame.getMainStoryBoard()
        let vc = storyboard.instantiateViewController(withIdentifier: parentViewControllerIdentifier) as! ParentMainViewController
        return vc
    }
}
