//
//  HMMManager.h
//  BeWarned
//
//  Created by ivan verenich on 8/18/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArtificialRecognizerManagerProtocol.h"
#import "ClusterizationManager.h"

@interface HMMManager : NSObject <ArtificialRecognizerManagerProtocol>

@property (assign, nonatomic) NSUInteger observationLength;
-(NSUInteger) recognize:(double**) image;

- (instancetype) initWithClusterizationManager:(id<ClusterizationManagerProtocol>) clusterManager;

@end
