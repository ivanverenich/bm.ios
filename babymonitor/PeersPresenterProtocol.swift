//
//  PeersPresenterProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import UIKit

protocol PeersPresenterProtocol: UITableViewDelegate, UITableViewDataSource {
    
    func getPeers()
    func onClosing()
    func closeDialog()
}
