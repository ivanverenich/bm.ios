//
//  MainInteractor.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import StoreKit

protocol MainInteractorCallBackProtocol: class {
    
    func startedCapturingVideo()
    func stopedCapturingVideo()
    func startedWithTrialInterval()
    func updateTrialTimeLeft(seconds: Int)
    func stopedWithTrialInterval()
    func gotNewImage(image: UIImage, motionRect: CGRect?)
}

let trialVersionMaxSeconds = 30 * 60

class MainInteractor: NSObject, MainInteractorProtocol {
    
    var analytics: AnalyticsProtocol?
    
    // in app purchase
    var products: [SKProduct]?

    // trial vars
    var trialVersionTimer: Timer?
    var currentSecondsLeft: Int = 0
    weak var delegate : MainInteractorCallBackProtocol?
    
    // detection service
    var detectionService: DetectionServiceProtocol?
    
    // notification service
    var notificationService: NotificationServiceProtocol?
    var didLostConnectionDuringStreaming: Bool = false
    
    //
    var isDetecting: Bool = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var settings: SettingsManager? {
        didSet {
            self.loadInAppPurchaseProductsIfNeed()
        }
    }
    
    // video capture + motion detect
    var videoCapture: VideoCaptureWithMotionDetectProtocol?
    var motionRect: CGRect?
    var motionDetectTime: Date?
    var frameCounter: Int = 0
    
    override init() {
        
        super.init()
        
        self.appDelegate.mpcManager.addDelegate(delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.settingsRemoteChanged(notification:)), name: NSNotification.Name(rawValue: observerRemoteEnabilityIdentifier), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.settingsVideoChanged(notification:)), name: NSNotification.Name(rawValue: observerVideoEnabilityIdentifier), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.appWillEnterBackgroundNotificationHandler), name: NSNotification.Name(rawValue: appWillEnterBackgroundNotificationName), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.appWillEnterForegroundNotificationHandler), name: NSNotification.Name(rawValue: appWillEnterForegroundNotificationName), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.settingsDidChangedNotificationHandler(notification:)), name: NSNotification.Name(rawValue: settingsDidChanged), object: nil)
        

        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.successPurchaseHandler), name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
            
        NotificationCenter.default.addObserver(self, selector: #selector(MainInteractor.errorPurchaseHandler), name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseErrorNotification), object: nil)
    }
    
    func updateNotificationMode(newMode: NotificationMode) {
        
        self.analytics!.sendEvent(event: AnalyticsEvents.ChangedSensitivityMode)
        self.settings!.currentMode = newMode
        self.detectionService!.notificationMode = newMode
        self.settings!.save()
    }
    
    func start() {
        
        isDetecting = true
        // check for trial period, if is trial set timer and max interval
        if (settings!.isFree) {
            currentSecondsLeft = trialVersionMaxSeconds
            trialVersionTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                                     target: self,
                                                     selector: #selector(MainInteractor.trialVersionCounter),
                                                     userInfo: nil,
                                                     repeats: true)
            if (delegate != nil) {
                delegate!.startedWithTrialInterval()
            }
        }
        
        if (videoCapture != nil) {
            
            if (settings!.enableRemote &&
                settings!.enableVideo &&
                appDelegate.mpcManager.foundPeers.count > 0) {
            
                delegate?.startedCapturingVideo()
                
                videoCapture!.delegate = self
                videoCapture!.start()
            }
            
            // this is only for test
//            if (delegate != nil) {
//                delegate!.startedCapturingVideo()
//            }
//            
//            videoCapture!.delegate = self
//            videoCapture!.start()
        }
        
        detectionService!.notificationMode = settings!.currentMode
        detectionService!.sensetivityLevel = Double(settings!.sensitiveModeSettings!.sensitivityValue!)
        detectionService!.delegate = self
        detectionService!.start()
    }
    
    func stop() {
        
        isDetecting = false
        
        if (videoCapture != nil) {
            delegate?.stopedCapturingVideo()
            videoCapture!.stop()
        }
        
        if (trialVersionTimer != nil) {
            trialVersionTimer!.invalidate()
            trialVersionTimer = nil
        }
        
        detectionService!.stop()
    }
    
    func rotateCamera() {
        
        videoCapture?.rotateCamera()
    }
    
    func getSharingViewController(view: UIView) -> UIActivityViewController? {
        
        let shareManager = ShareManager()
        shareManager.analytics = Analytics()
        return shareManager.share(viewForPresenting: view)
    }
    
    // MARK: - video stream control
    
    func startVideoIfPossible() {
        
        guard var videoCapt = self.videoCapture,
                settings!.enableRemote,
                settings!.enableVideo else {
            return
        }
        
        delegate?.startedCapturingVideo()
                
        videoCapt.delegate = self
        videoCapt.start()
    }
    
    func stopVIdeoIfPossible() {
        
        guard let videoCapt = videoCapture else {
            return
        }

        delegate?.stopedCapturingVideo()
        videoCapt.stop()
    }
    
    // MARK: - trial version count down
    
    func trialVersionCounter() {
        
        currentSecondsLeft = currentSecondsLeft - 1
        if (delegate != nil) {
            delegate!.updateTrialTimeLeft(seconds: currentSecondsLeft)
        }
        
        // currently will not send left time, until few updates
        
//        let peers = self.appDelegate.mpcManager.session.connectedPeers
//        if (settings!.enableRemote && peers.count > 0) {
//            var parametersDict: [String: AnyObject] = [:]
//            parametersDict["trialLeft"] = NSNumber.init(value: currentSecondsLeft)
//        
//            let data = NSKeyedArchiver.archivedData(withRootObject: parametersDict)
//        
//            if self.appDelegate.mpcManager.sendData(data: data, toPeer: peers) {
//                // do on sent
//            } else {
//                // do if not sent
//            }
//        }
        
        if (currentSecondsLeft <= 0) {
            self.stop()
            
            self.analytics!.sendEvent(event: AnalyticsEvents.DetectionStopedAuto)
            
            if (delegate != nil) {
                delegate!.stopedWithTrialInterval()
            }
        }
    }
    
}
