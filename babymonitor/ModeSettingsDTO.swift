//
//  ModeSettingsDTP.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class ModeSettingsDTO: NSObject, NSCoding {

    var enableFlash: Bool
    var enableVibration: Bool
    var enableSound: Bool
    var sensitivityValue: Float?
    var notificationMode: NotificationMode
    
    init(flash: Bool, vibration: Bool, sound: Bool, mode: NotificationMode) {
        
        enableFlash = flash
        enableVibration = vibration
        enableSound = sound
        notificationMode = mode
        
        super.init()
    }
    
    required convenience init?(coder decoder: NSCoder) {
        
        self.init(flash: true, vibration: true, sound: true, mode: NotificationMode.Sensitive)
        
        enableFlash = decoder.decodeBool(forKey: "MS_enableFlash")
        enableVibration = decoder.decodeBool(forKey: "MS_enableVibration")
        enableSound = decoder.decodeBool(forKey: "MS_enableSound")
        sensitivityValue = decoder.decodeFloat(forKey: "MS_sensitivityValue")
        notificationMode = NotificationMode(rawValue: decoder.decodeInteger(forKey: "MS_notificationMode"))!
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(enableFlash, forKey: "MS_enableFlash")
        aCoder.encode(enableVibration, forKey: "MS_enableVibration")
        aCoder.encode(enableSound, forKey: "MS_enableSound")
        if (sensitivityValue == nil) {
            let zero: Float = 0
            aCoder.encode(zero, forKey: "MS_sensitivityValue")
        } else {
            aCoder.encode(sensitivityValue!, forKey: "MS_sensitivityValue")
        }
        aCoder.encode(notificationMode.rawValue, forKey: "MS_notificationMode")
    }
}
