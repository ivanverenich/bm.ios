//
//  ShortExtension.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/3/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

extension Int16 {
    
    static var MIN = Int16.min
    static var MAX = Float.greatestFiniteMagnitude
    
}
