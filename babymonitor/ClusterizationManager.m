//
//  Clusterization.m
//  BeWarned
//
//  Created by ivan verenich on 8/17/15.
//  Copyright (c) 2015 ivan verenich. All rights reserved.
//

#import "ClusterizationManager.h"
#import "DDXML.h"
#import "Cluster.h"

@interface ClusterizationManager()

/**
 clusters count
 */
@property (assign, nonatomic) int k;

/**
 Clusters
 */
@property (strong, nonatomic) NSMutableArray* clusters;

@end

@implementation ClusterizationManager

-(instancetype)initWithFile:(NSString*)filename {

    self = [super init];
    if (self) {
        
        NSError* error = nil;
        NSString *xmlString = [NSString stringWithContentsOfFile:filename encoding:NSUTF8StringEncoding error:&error];
        DDXMLDocument *xmlDoc = [[DDXMLDocument alloc]initWithXMLString:xmlString options:0 error:&error];
        DDXMLElement *rootNode = [xmlDoc rootElement];
        
        [self deserializeFromXml:rootNode];
        
        rootNode = nil;
        xmlDoc = nil;
    }
    
    return self;
}

- (void) deserializeFromXml:(DDXMLElement*) node {
    
    _clusters = [NSMutableArray new];
    
    for (DDXMLElement* xmlCluster in node.children) {
        
        int clusterNumber = [[[xmlCluster attributeForName:@"number"]stringValue] intValue];
        
        DDXMLElement* xmlCentroid = (DDXMLElement*)[xmlCluster childAtIndex:0];
        NSMutableArray* centroidValues = [NSMutableArray new];
        for (DDXMLElement* xmlRow in xmlCentroid.children) {
            
            NSString* val = [xmlRow stringValue];
            val = [val stringByReplacingOccurrencesOfString:@"," withString:@"."];
            [centroidValues addObject:@([val floatValue])];
            
        }
        
        double* tempPoint = (double*)malloc(sizeof(double)*centroidValues.count);
        
        for (int i = 0; i < centroidValues.count; i++) {
            tempPoint[i] = [centroidValues[i] floatValue];
        }
        
        Pnt* p = [[Pnt alloc] initWithVector:centroidValues];
        Cluster* cluster = [[Cluster alloc] initWithNumber:clusterNumber centroid:p];
        
        [_clusters addObject:cluster];
    }
}

- (int)getClusterNumber:(Pnt*) point {
    
    int clusterNumber = 0;
    double minimalDistance = DBL_MAX;
    
    for (Cluster* cluster in _clusters) {
        
        double distance = [cluster.centroid getDistance:point];
        if (distance < minimalDistance) {
            
            minimalDistance = distance;
            clusterNumber = cluster.number;
        }
    }
    
    return clusterNumber;
}

@end
