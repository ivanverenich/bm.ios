//
//  MfccFrame.swift
//  SoundMonitor
//
//  Created by ivan verenich on 11/1/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

import Foundation
import Accelerate

@objc class MfccFrame: NSObject, FeatureFrameProtocol {
    
    /// Triangle filters count
    private static let triangleFiltersCount = 26
    
    /// Preemphasis coefficient
    private static let preEmphasisCoefficient = 0.97;
    
    /// Sampling rate
    private static let samplingRate = SampleRate
    
    /// Feature coefficient count
    private static let featureCoefficientCount: Int = Int(FeatureCoefficientCount)
    
    /// Liftering coefficient
    private static let lifteringCoefficient = 22.0
    
    /// Low boundary of analizing frequency in Hertz
    private static let lowHertzFrequency = 50.0
    
    /// High boundary of analizing frequency in Hertz
    private static let highHertzFrequency = 6000.0
    
    /// Hamming window
    private static var hammingWindow: [Double]?
    
    /// Liftering coefficients
    private static var lifteringCoefficients:[Double]?
    
    /// Cosine coefficients
    private static var cosineCoefficients: Array<Array<Double>>?
    
    /// Triangle mel filters
    private static var melFilters: Array<Array<Double>>?
    
    /// Source signal after all modifications in time scale
    private var sourceSignal: [Double]?
    
    /// Input signal
    private var inputSignal: [Float]
    
    /// Spectral coefficients
    private var spectrum: [Double]?
    
    /// Mel cepstral coefficients
    private var mfcc: [Double]?
    
    /// Constructor of mcff frame.Creates instanse and calculates all params of input signal
    ///
    /// - parameter buffer: input buffer
    ///
    /// - returns: Mfcc frame object
    init (buffer: [Float]) {
    
        inputSignal = buffer
        let singnalSize = MfccFrame.getAlignedWindowSize(singnalLength: buffer.count)
    
        MfccFrame.initLifteringCoefficients()
        MfccFrame.initCosineCoefficients()
        MfccFrame.initMelCoefficients(signalSize: singnalSize)
        MfccFrame.initHammingWindow(windowSize: singnalSize)
    
        sourceSignal = [Double](repeating: 0.0, count: singnalSize)
        
        super.init()
    
        let energyOfSignal = self.performPreEmphasisSignal(signal: buffer, true)
        spectrum = FFT(signal: sourceSignal!)
        let vMelSpectrum = melSpectrum(spectrum: spectrum!)
        var cepstrum = сepstrum(melSpectrum: vMelSpectrum)
        cepstrum[0] = energyOfSignal
        mfcc = cepstrum
    }
    
    convenience init(pointer: UnsafeMutablePointer<Float>, length: Int) {
        
        let buffer = SoundTools.convertUnsafePointerToArray(pointer: pointer, length: length)
        self.init(buffer: buffer)
    }
    
//    public MfccFrame(double[] cepstralCoefficients)
//    {
//    mfcc = cepstralCoefficients;
//    }
//

    
    /// Alignes signal size by 2 degree
    ///
    /// - parameter singnalLength: source signal length
    ///
    /// - returns: aligned singnal size to 2 degree
    private static func getAlignedWindowSize(singnalLength: Int) -> Int {
    
        var signalSize = 1
        while (signalSize < singnalLength) {
            signalSize *= 2
        }
    
        return signalSize
    }
    
    /// Performs misrepresentation of signal
    ///
    /// - parameter signal:              signal
    /// - parameter applyHammingWindown: should apply Hamming window
    ///
    /// - returns: energy of signal
    private func performPreEmphasisSignal(signal: [Float] , _ applyHammingWindow: Bool) -> Double {
        
        var energy = 0.0
        for i in 1 ..< signal.count {
            
            var preEmphasisResult = Double(signal[i]) - Double(MfccFrame.preEmphasisCoefficient) * Double(signal[i - 1])
            if (applyHammingWindow) {
                preEmphasisResult = preEmphasisResult * MfccFrame.hammingWindow![i-1]
            }
            sourceSignal![i] = preEmphasisResult
            energy = energy + sourceSignal![i] * sourceSignal![i]
        }
        
        let result = log(CommonTools.Max(a: energy / Double((signal.count - 1)), Double.MIN))
    
        return result
    }
    
    /// Performs fast Fourier transforn
    ///
    /// - parameter signal: input signal
    ///
    /// - returns: spectrum
    private func FFT(signal: [Double]) -> [Double] {
        
        var spectrum = [Double](repeating: 0.0, count: signal.count / 2 + 1)
        //let fftResult = MfccFrame.calculateSpectrum(signal)
        
        var real = [Double](signal)
        var imaginary = [Double](repeating: 0.0, count: signal.count)
        var splitComplex = DSPDoubleSplitComplex(realp: &real, imagp: &imaginary)
        
        let length = vDSP_Length(floor(log2(Float(signal.count))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetupD(length, radix)
        vDSP_fft_zipD(weights!, &splitComplex, 1, length, FFTDirection(FFT_FORWARD))
        
        let realPart = SoundTools.convertUnsafePointerToArray(pointer: splitComplex.realp, length: signal.count)
        let imagPart = SoundTools.convertUnsafePointerToArray(pointer: splitComplex.imagp, length: signal.count)
        
        vDSP_destroy_fftsetupD(weights)

        let tempVal = 1.0 / (Double(signal.count))
        
        var realVal = realPart[0]
        var imagVal = imagPart[0]
    
        spectrum[0] = realVal * realVal + imagVal * imagVal
        spectrum[0] = spectrum[0] * tempVal
    
        let index = signal.count / 2
        
        for i in 1 ..< index {
    
            realVal = realPart[i]
            imagVal = imagPart[i]
            spectrum[i] = tempVal * 2 * (realVal * realVal + imagVal * imagVal)
        }
    
        realVal = realPart[index]
        imagVal = imagPart[index]
        spectrum[index] = (realVal * realVal + imagVal * imagVal) * tempVal;
    
        return spectrum
    }
    
    /// Calculates spectrum using accelerate framework
    ///
    /// - parameter input: input signal
    ///
    /// - returns: DSPDoubleSplitComplex struct (contains real and imagenary parts)
    static func calculateSpectrum(input: [Double]) -> DSPDoubleSplitComplex {
        
        var real = [Double](input)
        var imaginary = [Double](repeating: 0.0, count: input.count)
        var splitComplex = DSPDoubleSplitComplex(realp: &real, imagp: &imaginary)
        
        let length = vDSP_Length(floor(log2(Float(input.count))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetupD(length, radix)
        vDSP_fft_zipD(weights!, &splitComplex, 1, length, FFTDirection(FFT_FORWARD))
        
        vDSP_destroy_fftsetupD(weights)
        
        return splitComplex
    }
    
    /// Translates spectrum to mel scale
    ///
    /// - parameter spectrum: spectrum
    ///
    /// - returns: spectrum in mel scale
    private func melSpectrum(spectrum: [Double]) -> [Double] {
        
        var melSpectrum = [Double](repeating: 0.0, count: MfccFrame.triangleFiltersCount)
    
        for i in 0 ..< MfccFrame.triangleFiltersCount {
            melSpectrum[i] = 0.0
            for j in 0 ..< MfccFrame.melFilters![i].count {
                melSpectrum[i] += spectrum[j] * MfccFrame.melFilters![i][j]
            }
            melSpectrum[i] = log(CommonTools.Max(a: melSpectrum[i], Double.MIN))
        }
    
        return melSpectrum
    }
    
    /// Calculates cepstrum
    ///
    /// - parameter melSpectrum: spectrum in mel scale
    ///
    /// - returns: cepstrum
    private func сepstrum(melSpectrum: [Double]) -> [Double] {
        
        var cepstrum = [Double](repeating: 0.0, count: MfccFrame.featureCoefficientCount)
        for i in 0 ..< MfccFrame.featureCoefficientCount {
            var temp_val = 0.0
            for j in 0 ..< MfccFrame.triangleFiltersCount {
                temp_val = temp_val + melSpectrum[j] * MfccFrame.cosineCoefficients![i][j]
            }
            temp_val *= 2.0
            if (i > 0) {
                temp_val = temp_val / sqrt(4.0 * Double(MfccFrame.triangleFiltersCount))
            } else {
                temp_val = temp_val / sqrt(2.0 * Double(MfccFrame.triangleFiltersCount))
            }
    
            cepstrum[i] = temp_val * MfccFrame.lifteringCoefficients![i]
        }
    
        return cepstrum
    }
    
    /// Fills liftering coefficients
    ///
    /// - returns: void
    static func initLifteringCoefficients() {
    
        guard lifteringCoefficients == nil else {
            return
        }
    
        lifteringCoefficients = [Double](repeating: 0.0, count: MfccFrame.featureCoefficientCount)

        for i in 0 ..< MfccFrame.featureCoefficientCount {
            
            let sinV = sin(Double(i) * Double.pi / MfccFrame.lifteringCoefficient)
            let liftCoeff = 1.0 + 0.5 * MfccFrame.lifteringCoefficient * sinV
            lifteringCoefficients![i] = liftCoeff
        }
    }
    
    /// Fills cosine coefficients
    static func initCosineCoefficients() {
    
        guard cosineCoefficients == nil else {
            return
        }
    
        cosineCoefficients = Array<Array<Double>>()
        
        for _ in 0 ..< MfccFrame.featureCoefficientCount {
            cosineCoefficients!.append(Array(repeating:0.0, count:MfccFrame.triangleFiltersCount))
        }
    
        for i in 0 ..< MfccFrame.featureCoefficientCount {
            for j in 0 ..< MfccFrame.triangleFiltersCount {
                cosineCoefficients![i][j] = cos(Double.pi * (2 * Double(j) + 1) / (2.0 * Double(MfccFrame.triangleFiltersCount)) * Double(i))
            }
        }
    }
    
    /// Fills triangle mel filters
    ///
    /// - parameter signalSize: size of signal in samples
    ///
    static func initMelCoefficients(signalSize: Int) {
        
        guard melFilters == nil else {
            return
        }
    
        let minMel = SoundTools.frequencyToMel(value: MfccFrame.lowHertzFrequency)
        let maxMel = SoundTools.frequencyToMel(value: MfccFrame.highHertzFrequency)
        var curMel = minMel
        let melInterval = (maxMel - minMel) / (Double(MfccFrame.triangleFiltersCount) + 1.0)
        var prev_freq: Double
        var cur_freq: Double
        var next_freq: Double
        
        var prev_freq_index: Int
        var cur_freq_index:Int
        var next_freq_index: Int
    
        melFilters = Array<Array<Double>>()
            
        for _ in 0 ..< MfccFrame.triangleFiltersCount {
            melFilters!.append(Array(repeating:0.0, count:signalSize / 2 + 1))
        }
    
        for i in 0 ..< MfccFrame.triangleFiltersCount {
            
            prev_freq = SoundTools.melToFrequency(value: curMel)
            cur_freq = SoundTools.melToFrequency(value: curMel + melInterval)
            next_freq = SoundTools.melToFrequency(value: curMel + 2 * melInterval)
    
            prev_freq_index = Int(round((Double(signalSize) * prev_freq) / Double(MfccFrame.samplingRate)))
            cur_freq_index = Int(round((Double(signalSize) * cur_freq) / Double(MfccFrame.samplingRate)))
            next_freq_index = Int(round((Double(signalSize) * next_freq) / Double(MfccFrame.samplingRate)))
    
            for j in 0 ... (signalSize / 2) {
    
                if ((prev_freq_index <= j) && (j <= cur_freq_index)) {
                    
                    melFilters![i][j] = (Double(j) - Double(prev_freq_index)) / (Double(cur_freq_index) - Double(prev_freq_index))
                    
                } else if ((cur_freq_index < j) && (j <= next_freq_index)) {
    
                    melFilters![i][j] = (Double(j) - Double(next_freq_index)) / (Double(cur_freq_index) - Double(next_freq_index))
                
                } else {
                    
                    melFilters![i][j] = 0.0
                }
            }
    
            curMel = curMel + melInterval
        }
    }
    
    /// Fills Hamming window
    ///
    /// - parameter windowSize: window size
    ///
    static func initHammingWindow(windowSize: Int) {
    
        guard hammingWindow == nil else {
            return
        }
    
        hammingWindow = [Double](repeating: 0.0, count: windowSize - 1)
    
        for i in 0 ..< (windowSize - 1) {
            
            hammingWindow![i] = 0.54 - 0.46 * cos(2.0 * Double.pi * Double(i) / (Double(windowSize) - 2.0))
        }
    }
    
    /// Gets FFT coefficients
    ///
    /// - returns: array of FFT coefficients
    func getFFTCoefficients() -> [Double] {
        return self.spectrum!
    }
    
    /// Gets input signal
    ///
    /// - returns: float array
    func getSourceBuffer() -> [Float] {
        return self.inputSignal
    }
    
    
    /// Gets mfcc coefficients
    ///
    /// - returns: array of mfcc coefficients
//    func getFeatureCoefficietns() -> [Double] {
//        return self.mfcc!
//    }
    
    func getFeatureCoefficietns() -> [NSNumber]! {
        
        var coefficients: [NSNumber] = []
        for mfccCoefficient in self.mfcc! {
            coefficients.append(NSNumber.init(value: mfccCoefficient))
        }
        return coefficients
    }
}
