//
//  AudioProcessorProtocol.h
//  SoundMonitor
//
//  Created by ivan verenich on 11/7/16.
//  Copyright © 2016 ivan verenich. All rights reserved.
//

#ifndef AudioProcessorProtocol_h
#define AudioProcessorProtocol_h

@protocol AudioProcessorCallBackProtocol <NSObject>

@optional
-(void)OnDataAvailable:(SInt16*) data withLength:(int)length;

@end

@protocol AudioProcessorProtocol <NSObject>

@required
@property (nonatomic, strong) id<AudioProcessorCallBackProtocol> delegate;
- (void) start;
- (void) stop;

@end

#endif /* AudioProcessorProtocol_h */
