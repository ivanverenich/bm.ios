//
//  ModelModulesConnectionProtocols.h
//  BeWarned
//
//  Created by Admin on 25.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#ifndef BeWarned_ModelModulesConnectionProtocols_h
#define BeWarned_ModelModulesConnectionProtocols_h


#endif

#import <Foundation/Foundation.h>
#import "DDXML.h"

// протокол для объектов, поддерживающих xml сериализацию

@protocol XmlSerializableProtocol <NSObject>

    @required
        -(void)DeserializeFromXml:(DDXMLElement*)node;

@end


@end
