//
//  MainPresenter.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class MainPresenter: NSObject, MainPresenterProtocol, MainInteractorCallBackProtocol {

    var wireframe: MainWireframeProtocol?
    var view: MainViewProtocol?
    var notificationMode: NotificationMode = NotificationMode.Sensitive
    var interactor: MainInteractorProtocol? {
        willSet(newValue) {
            notificationMode = newValue!.settings!.currentMode
        }
    }
    var currentMode: NotificationMode {
        get { return notificationMode }
        set(newValue) {
            notificationMode = newValue
            interactor!.updateNotificationMode(newMode: notificationMode)
        }
    }
    
    func startStopDetection() {
     
        if (!interactor!.isDetecting) {
            self.interactor!.start()
            
        } else {
            self.interactor!.stop()
        }
        self.view?.updateInterface(isDetecting: interactor!.isDetecting)
    }
    
    func openSettings() {
        
        wireframe!.openSettingsDialog(settings: interactor!.settings!)
    }
    
    func openSharing(view: UIView) {
    
        let sharingVC = interactor!.getSharingViewController(view: view)
        let presenterVC = self.view as! UIViewController
        self.wireframe!.presentSharingDialog(presenterVC: presenterVC, sharingVC: sharingVC!)
    }
    
    func upgrade() {
     
        self.interactor!.purchase()
    }
    
    func restore() {
        
        self.interactor!.restore()
    }
    
    func rotateCamera() {
        
        self.interactor!.rotateCamera()
    }
    
    // MARK - TrialVersionProtocol
    
    func startedWithTrialInterval() {
        self.view!.startedWithTrialInterval()
    }
    
    func updateTrialTimeLeft(seconds: Int) {
        self.view!.updateTrialTimeLeft(seconds: seconds)
    }
    
    func stopedWithTrialInterval() {
        self.view!.stopedWithTrialInterval()
    }
    
    func startedCapturingVideo() {
        
        //self.view!.addVideoPreview(layer: self.interactor!.videoCapture!.captureLayer)
        self.view!.addVideoPreview(view: self.interactor!.videoCapture!.filterView!)
    }
    
    func stopedCapturingVideo() {
        
        //self.view!.removeVideoPreview(layer: self.interactor!.videoCapture!.captureLayer)
        self.view!.removeVideoPreview(view: self.interactor!.videoCapture!.filterView!)
    }
    
    func gotNewImage(image: UIImage, motionRect: CGRect?) {
        
        self.view!.updateCapturedImage(image: image, motionRect: motionRect)
    }
}
