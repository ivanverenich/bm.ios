//
//  MainViewProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/29/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

protocol MainViewProtocol {
    
    func updateInterface(isDetecting: Bool)
    func addVideoPreview(layer: AVCaptureVideoPreviewLayer)
    func removeVideoPreview(layer: AVCaptureVideoPreviewLayer)
    
    func addVideoPreview(view: UIView)
    func removeVideoPreview(view: UIView)
    
    func startedWithTrialInterval()
    func updateTrialTimeLeft(seconds: Int)
    func stopedWithTrialInterval()
    
    func updateCapturedImage(image: UIImage, motionRect: CGRect?)
}
