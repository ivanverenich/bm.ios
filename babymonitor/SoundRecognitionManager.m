//
//  SoundRecognitionManager.m
//  BeWarned
//
//  Created by Admin on 28.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "SoundRecognitionManager.h"
#import "SoundTecnique.h"
#import "babymonitor-Swift.h"
#import "ModelConstants.h"

@interface SoundRecognitionManager()

@property (strong, nonatomic) id<ArtificialRecognizerManagerProtocol> recognizer;

@end

@implementation SoundRecognitionManager
    
-(id)initWithRecognazer:(id<ArtificialRecognizerManagerProtocol>)recognizer {
    
    self = [super init];
    if (self) {
        
        _recognizer = recognizer;
    }
    
    return self;
}

/**
 Recognizes dangerous event in feature collection

 @param featureCollection
 @return recognized dangerous event
 */
-(DangerousEvents)recognize:(id<FeatureCollectionProtocol>)featureCollection {
 
    double** image = [self getAdapatedImage:featureCollection];
    return [self recognizeImage:image noise:featureCollection.isNoisy];
}

/**
 Recognizes input two dimension image using recognizer

 @param image input image
 @param isNoisy mark that input image is noisy
 @returns recognized dangerous event
 */
-(DangerousEvents)recognizeImage:(double**) image noise:(BOOL)isNoisy {
    
    if (isNoisy) {
        
        return Noise;
    }
    
    DangerousEvents recognizedEvent = [SoundClasses getDangerousEventByIndex:[_recognizer recognize:image]];
    NSLog(@"HMM sound class is %lu", (unsigned long)recognizedEvent);
    
    free(image);
    
    return recognizedEvent;
}

- (double**) getAdapatedImage:(id<FeatureCollectionProtocol>) featureCollection {
    
    int sizeX = (int)featureCollection.featureFrames.count;
    int sizeY = FeatureCoefficientCount;
    
    double** image = (double**)malloc(sizeof(double*) * sizeX);
    
    for (int x = 0; x < sizeX; x++) {
        
        image[x] = (double*)malloc(sizeof(double) * sizeY);
        MfccFrame* currentFrame = featureCollection.featureFrames[x];
        
        NSArray<NSNumber*>* mfccCoefficients = [currentFrame getFeatureCoefficietns];
        
        for (int y = 0; y < sizeY; y++)
        {
            image[x][y] = [mfccCoefficients[y] doubleValue];
        }
    }
    
    return image;
}

@end
