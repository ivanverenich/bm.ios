//
//  PeersViewController.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit

class PeersViewController: UIViewController, PeersViewProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIBarButtonItem!

    
    var presenter: PeersPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backButton.image = UIImage(named: "ArrowBack")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tableView.delegate = self.presenter!
        tableView.dataSource = self.presenter!
        self.presenter!.getPeers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        self.presenter!.onClosing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - PeersViewProtocol
    
    func updatePeers() {
        
        tableView.reloadData()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        presenter!.closeDialog()
    }
    
}
