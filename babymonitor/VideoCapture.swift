//
//  VidepCapture.swift
//  babymonitor
//
//  Created by ivan verenich on 12/1/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import UIKit
import AVFoundation

enum CaptureErrors: Error {
    
    case lockingConfiguration
    case capturingInputDevice
}

protocol BaseVideoCaptureProtocol {
    
    func start()
    func stop()
}

protocol VideoCaptureProtocol : BaseVideoCaptureProtocol {
    var outputManager: CustomCaptureOutput? { get }
    var captureLayer: AVCaptureVideoPreviewLayer! { get }
}

class VideoCapture: NSObject, VideoCaptureProtocol {

    var captureSession: AVCaptureSession!
    var captureLayer: AVCaptureVideoPreviewLayer!
    private var videoDevice: AVCaptureDevice?
    var outputManager: CustomCaptureOutput?
    
    init?(captureOutput: CustomCaptureOutput) {
        
        outputManager = captureOutput
        captureSession = AVCaptureSession()
     
        captureLayer = AVCaptureVideoPreviewLayer.init(session: captureSession)
        captureLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        captureLayer.frame = CGRect.init(x: 0, y: 0, width: 100, height: 100)

        // Create video device input
        videoDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)

        super.init()
        
        do {
            let videoDeviceInput = try AVCaptureDeviceInput.init(device: videoDevice)
            captureSession.addInput(videoDeviceInput)
            
            // add output
            captureSession.addOutput(captureOutput)
            
            try self.setFrameRate(framerate: 5, onDevice: videoDevice!)
            
        } catch {
            
            return nil
        }
    }
    
    private func setFrameRate(framerate: Int, onDevice: AVCaptureDevice) throws {
    
        do {
            try onDevice.lockForConfiguration()
  
            onDevice.activeVideoMaxFrameDuration = CMTimeMake(1,Int32(framerate))
            onDevice.activeVideoMinFrameDuration = CMTimeMake(1,Int32(framerate))
            onDevice.unlockForConfiguration()
            
        } catch {
            throw CaptureErrors.lockingConfiguration
        }
    }
    
    func start() {
        if (videoDevice != nil) {
            captureSession.startRunning()
        }
    }
    
    func stop() {
        captureSession.stopRunning()
    }
}
