//
//  SettingsInteractorProtocol.swift
//  babymonitor
//
//  Created by ivan verenich on 11/30/16.
//  Copyright © 2016 beWarned. All rights reserved.
//

import Foundation

protocol SettingsInteractorProtocol {
    
    var analytics: AnalyticsProtocol? { get set }
    var settings: SettingsManager? { get set }
    func getProductsWithComplitionHandler(handler: @escaping ProductsRequestUICompletionHandler)
    func purchase()
    func restore()
    func getContactEmail() -> String
    func getContactSubject() -> String
}
